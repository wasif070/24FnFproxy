/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package bytemanipulation;

/**
 *
 * @author reefat
 */
public class ByteManipulation {
//-------------------------------------------------------------------

    public static boolean startWithByte(byte[] largerArrayOfByte, byte[] smallerArrayOfByte) {
        //System.out.println("(P) ==> startWithByte  --> " + new String(largerArrayOfByte).startsWith(new String(smallerArrayOfByte)) );
        for (int index = 0; index < smallerArrayOfByte.length; index++) {
            if (smallerArrayOfByte[index] != largerArrayOfByte[index]) {
                //System.err.println("O ==> false");
                return false;
            }
        }
        //System.err.println("O ==> "+true);
        return true;
    }

//-------------------------------------------------------------------
    public static boolean startWithIndexByte(byte[] largerArrayOfByte, byte[] smallerArrayOfByte, int i_index) {
        if ((largerArrayOfByte.length - i_index) < smallerArrayOfByte.length) {
            return false;
        }
        for (int index = 0; index < smallerArrayOfByte.length; index++) {
            if (smallerArrayOfByte[index] != largerArrayOfByte[index + i_index]) {
                return false;
            }
        }
        return true;
    }

//-------------------------------------------------------------------
    public static byte[] concatByte(byte[] firstArrayOfByte, byte[] secondArrayOfByte) {
        //System.out.println("P ==>  concatByte  -->  " + new String(firstArrayOfByte).concat(new String(secondArrayOfByte)));
        byte[] temp_array = new byte[firstArrayOfByte.length + secondArrayOfByte.length];
        System.arraycopy(firstArrayOfByte, 0, temp_array, 0, firstArrayOfByte.length);
        System.arraycopy(secondArrayOfByte, 0, temp_array, firstArrayOfByte.length, secondArrayOfByte.length);
        //System.err.println("O ==> "+new String(temp_array));
        return temp_array;
    }

//-------------------------------------------------------------------
    public static int indexInByteArray(byte[] byteArray, int startPoint, char targetChar) {
        //System.out.println("P ==> "+new String(byteArray).indexOf(targetChar,startPoint));
        for (int index = startPoint; index < byteArray.length; index++) {
            if ((char) byteArray[index] == targetChar) {
                //System.err.println("O ==> "+index);
                return index;
            }
        }
        //System.err.println("O ==> -1");
        return -1;
    }

//-------------------------------------------------------------------
    public static int indexOfByteInByteArray(byte[] byteArray, int startPoint, byte[] targetByte) {
        int i;
        //System.out.println("P ==> "+new String(byteArray).indexOf(new String(targetByte), startPoint));
        if (targetByte.length < 1) {
            if (byteArray.length <= startPoint) {
                //System.err.println("O ==> "+byteArray.length);
                return byteArray.length;
            } else {
                //System.err.println("O ==> "+startPoint);
                return startPoint;
            }
        }
        for (int index = 0; index < byteArray.length - startPoint; index++) {
            if (byteArray[index + startPoint] == targetByte[0]) {
                for (i = 0; i < targetByte.length; i++) {
                    if (byteArray[index + startPoint + i] != targetByte[i]) {
                        break;
                    }
                }

                if (i == targetByte.length) {
                    //System.err.println("O ==> "+index + startPoint);
                    return index + startPoint;
                }
            }
        }
        //System.err.println("O ==> -1");
        return -1;
    }

//-------------------------------------------------------------------
    public static byte[] subStringInByteArray(byte[] byteArray, int startPoint, int endPoint) {
        if (byteArray.length < endPoint) {
            throw new IndexOutOfBoundsException("Array index Out of Bound");
        }
        int size = endPoint - startPoint;
        byte[] str_byte2 = new byte[size];
        for (int i = 0; i < size; i++) {
            try {
                str_byte2[i] = byteArray[i + startPoint];
            } catch (Exception e) {
            }
        }
        return str_byte2;
    }

//-------------------------------------------------------------------    
    public static boolean equalsIgnoreCase(byte[] arrayOne, byte[] arrayTwo) {

        if (arrayOne.length != arrayTwo.length) {
            return false;
        }
        for (int i = 0; i < arrayOne.length; i++) {
            if (isLetter(arrayOne[i]) && isLetter(arrayTwo[i])) {
                if (((arrayOne[i] - arrayTwo[i]) != 0) && ((arrayOne[i] - arrayTwo[i]) != 32) && ((arrayOne[i] - arrayTwo[i]) != -32)) {
                    return false;
                }
            } else {
                if (arrayOne[i] != arrayTwo[i]) {
                    return false;
                }
            }
        }

        return true;
    }

//-------------------------------------------------------------------    
    private static boolean isLetter(byte param_b) {
        if (param_b < 65 || param_b > 122) {
            return false;
        }
        if (param_b > 90 && param_b < 97) {
            return false;
        }
        return true;
    }

//-------------------------------------------------------------------    
    private static boolean isLetterOrNumber(byte param_b) {
        if (param_b < 48 || param_b > 122) {
            return false;
        }

        if (param_b > 57 && param_b < 65) {
            return false;
        }

        if (param_b > 90 && param_b < 97) {
            return false;
        }
        return true;
    }

//-------------------------------------------------------------------
    public static byte[] new_byte(byte[] arrayOfByte, int offset, int len) {
        byte[] tempByte = new byte[len];
        System.arraycopy(arrayOfByte, offset, tempByte, 0, len);
        return tempByte;
    }

    public static byte[] toLowerByte(byte[] inputByteArray) {
        for (int i = 0; i < inputByteArray.length; i++) {
            if (inputByteArray[i] >= 65 && inputByteArray[i] <= 90) {
                inputByteArray[i] += 32;
            }
        }
        return inputByteArray;
    }

//-------------------------------------------------------------------
//    public static byte[] getHeaderValue(byte[] byteArray, byte[] targetByte) {
//        int i;
//        if (targetByte.length < 1) {
//            return null;
//        }
//        for (int index = 0; index < byteArray.length; index++) {
//            if (byteArray[index] == targetByte[0]) {
//                if (isFullMatched(byteArray, index, targetByte)) {
//                    int first = 0;
//                    int second = 0;
//
//                    for (int j = index + targetByte.length; j < byteArray.length; j++) {
//                        if (!isLetterOrNumber(byteArray[j])) {
//                            continue;
//                        }
//                        first = j;
//                        break;
//                    }
//
//                    for (int j = first; j < byteArray.length; j++) {
//                        if (isLetterOrNumber(byteArray[j])) {
//                            continue;
//                        }
//                        second = j;
//                        System.err.println("  first  " + first + " second " + second);
//                        return subStringInByteArray(byteArray, first, second);
//                    }
//                }
//
//            }
//        }
//        return null;
//    }

    public static byte[] getHeaderValue(byte[] byteArray, byte[] targetByte) {
        int i,inner_index;
        byte[] ss = new byte[1];
        if (targetByte.length < 1) {
            return null;
        }
        for (int index = 0; index < byteArray.length; index++) {
            if (byteArray[index] == targetByte[0]) {
                if (isFullMatched(byteArray, index, targetByte)) {
                    int first = 0;
                    int second = 0;

                    for (int j = index + targetByte.length; j < byteArray.length; j++) {
                        ss[0] = byteArray[j];
                        System.out.println("--" + new String(ss));

                        if (!isQuotation(byteArray[j])) {
                            continue;
                        }

                        for (inner_index = ++j; inner_index < byteArray.length; inner_index++) {
                            
                            if (!isQuotation(byteArray[inner_index])) {
                                continue;
                            }
                            
                            break;
                        }
                        j = inner_index;
                        first = ++j;

                        break;
                    }

                    for (int j = first; j < byteArray.length; j++) {
                        ss[0] = byteArray[j];
                        System.out.println("--" + new String(ss));
                        if (isQuotation(byteArray[j])) {
                            second = j;
                            System.err.println("  first  " + first + " second " + second);
                            return subStringInByteArray(byteArray, first, second);
                        }

                    }
                }

            }
        }
        return null;
    }

//-------------------------------------------------------------------    
    private static boolean isQuotation(byte param_b) {
        if (param_b == 34) {
            return true;
        }
        return false;
    }

    public static boolean isFullMatched(byte[] largerArrayOfBytes, int index, byte[] smallerArrayOfBytes) {
        for (int i = 0; i < smallerArrayOfBytes.length; i++) {
            if (largerArrayOfBytes[i + index] != smallerArrayOfBytes[i]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        byte[] byteArray = "{\"action\":\"RESPONSE\",\"packetId\":\"a234b_c234@d\"}".getBytes();
        byte[] targetBytes = "packetId".getBytes();
        System.arraycopy(targetBytes, 2, targetBytes, 0, targetBytes.length-2);
//        System.out.println(" ==> " + new String(getHeaderValue(byteArray, targetBytes)));
        System.out.println(""+new String(targetBytes));
    }
}
