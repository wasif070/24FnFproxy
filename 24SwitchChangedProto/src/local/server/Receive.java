/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package local.server;

import java.net.*;


import java.io.*;
import java.util.logging.Level;
import org.apache.log4j.Logger;

public class Receive extends Thread {

    Logger logger = Logger.getLogger(Receive.class.getName());
    LocationService location_service = null;
    AuthenticationService authentication_service = null;
    int s_port = 0;


    public Receive(LocationService locationService, int port) {
        this.location_service = locationService;
        this.s_port = port;
    }

    public Receive(AuthenticationService authenticationService, int port) {
        this.authentication_service = authenticationService;
        this.s_port = port;
    }

    public void run() {

        try {
            DatagramSocket serverSocket = new DatagramSocket(s_port);
            byte[] receiveData = new byte[1024];
            while (true) {
                try {                    
                    DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                    serverSocket.receive(receivePacket);
                    serverSocket.setSoTimeout(3000);
                    String line = new String(receivePacket.getData());
                    receiveData = new byte[1024];
                    System.out.println("RECEIVED: " + line.trim());
                   /* if ("update_live_users".equals(line.trim())) {
                        Send.sendRegisteredUsers(receivePacket.getAddress(), receivePacket.getPort());
                    }
                    if (location_service != null) {

                        String user = null;
                        String str[] = line.split("\n");
                        for (int i = 0; i < str.length; i++) {
                            if (str[i] == null) {
                                break;
                            }
                            if (str[i].startsWith("To")) {
                                Parser par = new Parser(str[i]);
                                user = par.skipString().getString();
                                location_service.addUser(user);

                                for (Enumeration e = location_service.getUserContactURLs(user); e.hasMoreElements();) {  // forcefully remove contact only
                                    String contact = (String) e.nextElement();
                                    location_service.removeUserContact(user, contact);
                                }
                                continue;
                            }
                            if (str[i].startsWith(SipHeaders.Contact)) {
                                SipParser par = new SipParser(str[i]);
                                NameAddress name_address = ((SipParser) par.skipString()).getNameAddress();
                                String expire_value = par.goTo("expires=").skipN(8).getStringUnquoted();
                                if (expire_value.equalsIgnoreCase("NEVER")) {
                                    location_service.addUserStaticContact(user, name_address);
                                } else {
                                    Date expire_time = (new SipParser(expire_value)).getDate();
                                    location_service.addUserContact(user, name_address, expire_time);
                                }
                                continue;
                            }
                            if (str[i].startsWith("Unreg")) {
                                Parser par = new Parser(str[i]);
                                user = par.skipString().getString();
                                if (location_service.hasUser(user)) {
                                    for (Enumeration e = location_service.getUserContactURLs(user); e.hasMoreElements();) {
                                        String contact = (String) e.nextElement();
                                        location_service.removeUserContact(user, contact);
                                    }
                                    location_service.removeUser(user);
                                    continue;
                                }
                            }
                        }
                        location_service.forceSync();
                    }*/
                    if (authentication_service != null) {
                        if (line.startsWith("all")) {
                            String[] auth_info = line.trim().split(",");                           
                            authentication_service.addUser(auth_info[1].trim(), auth_info[2].trim().getBytes());
                            Send.sendConfirmationMsg("cf:"+auth_info[3].trim(), ServerProfile.auth_domains, ServerProfile.auth_port, serverSocket);
                        }
                        if (line.startsWith("add")) {
                            String[] auth_info = line.trim().split(",");
                            authentication_service.addUser(auth_info[1].trim(), auth_info[2].trim().getBytes());
                            Send.sendConfirmationMsg("cf:"+auth_info[3].trim(), ServerProfile.auth_domains, ServerProfile.auth_port, serverSocket);
                        }
                        if (line.startsWith("edit")) {
                            String[] auth_info = line.trim().split(",");
                            authentication_service.removeUser(auth_info[3].trim());
                            authentication_service.addUser(auth_info[1].trim(), auth_info[2].trim().getBytes());
                            Send.sendConfirmationMsg("cf:"+auth_info[5].trim(), ServerProfile.auth_domains, ServerProfile.auth_port, serverSocket);
                        }
                        if (line.startsWith("delete")) {
                            String[] auth_info = line.trim().split(",");
                            authentication_service.removeUser(auth_info[1].trim());
                            Send.sendConfirmationMsg("cf:"+auth_info[2].trim(), ServerProfile.auth_domains, ServerProfile.auth_port, serverSocket);
                        }
                        //authentication_service.forceSync();
                    }
                } catch (IOException ex) {              
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException ex1) {
                        java.util.logging.Logger.getLogger(Receive.class.getName()).log(Level.SEVERE, null, ex1);
                    }
                } catch(Exception e){
                    logger.debug("Error in Auth Receive: " + e.getMessage());
                }
            }

        } catch (IOException ex) {
            logger.debug("Error in receive: " + ex.getMessage());

        }
    }
}
