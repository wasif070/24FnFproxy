/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package local.server;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.sql.PreparedStatement;
import java.util.Enumeration;
import java.util.logging.Level;
import org.zoolu.tools.DateFormat;

public class CleanExpiredData extends Thread {

    LocationService location_service = null;
    PreparedStatement ps = null;
    long TWO_MINUTES = 120000;
    long EXPIRE_TIME = 30000;
    long execution_time = 0;

    public CleanExpiredData(LocationService locationService, int expire_time) {
        this.location_service = locationService;
        EXPIRE_TIME = expire_time;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (System.currentTimeMillis() - execution_time >= EXPIRE_TIME) {
                   delete_expired();
                   forece_delete_expired();
                    execution_time = System.currentTimeMillis();
                }
                this.sleep(EXPIRE_TIME);
            } catch (Exception ex) {
            }
        }
    }

    public void delete_expired() {
        try {
            long exp_chk = System.currentTimeMillis();
            String sql = "";
            for (Enumeration e = location_service.getUsers(); e.hasMoreElements();) {
                String user = (String) e.nextElement();
                for (Enumeration p = location_service.getUserContactURLs(user); p.hasMoreElements();) {
                    String contact = (String) p.nextElement();
                    long expires = (long) location_service.getUserContactExpirationDate(user, contact).getTime();

                    if (exp_chk > expires) {
                        location_service.removeUserContact(user, contact);
                        location_service.removeUser(user);

                        String[] temp_checking_local_ip = user.split("@");
                        Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
                        boolean local_ip_mached = false;
                        for (; n.hasMoreElements();) {
                            NetworkInterface ne = n.nextElement();
                            Enumeration<InetAddress> a = ne.getInetAddresses();
                            for (; a.hasMoreElements();) {
                                InetAddress addr = a.nextElement();
                                if (addr.getHostAddress().toString().equals(temp_checking_local_ip[1])) {
                                    local_ip_mached = true;
                                    break;
                                }
                            }
                        }
                        if (local_ip_mached) {
                            /*
//---------------------------------added by reefat----------------------------------------------------
                            //name_address.change_url(msg.getRemoteAddress(), msg.getRemotePort());
                            String[] temp = contact.trim().split(":");
                            int ind = 0;
                            for (ind = 0; ind < temp[2].length(); ind++) {
                                try {
                                    Double.parseDouble(String.valueOf(temp[2].charAt(ind)));
                                } catch (NumberFormatException ex) {
                                    break;
                                }
                            }
                            contact = temp[0] + ":" + temp[1] + ":" + temp[2].substring(0, ind);
//---------------------------------------end of addition----------------------------------------------  
*/
                            sql = "delete from live_users where contact = ?";
                            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
                            ps.setString(1, contact);
                            ps.executeUpdate();
                            ps.close();
                        }
                    }
                }
            }
            location_service.forceSync();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CleanExpiredData.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }

        }
    }

    public void forece_delete_expired() {
        try {
            String sql = "delete from live_users where expire_time_in_timestamp < '" + DateFormat.getTimeStamp(System.currentTimeMillis() - EXPIRE_TIME) + "'";
            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
            ps.executeUpdate();
            ps.close();

            location_service.forceSync();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(CleanExpiredData.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
            }

        }
    }
}
