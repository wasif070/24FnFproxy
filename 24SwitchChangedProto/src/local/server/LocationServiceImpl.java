/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This source code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package local.server;

import java.util.logging.Level;
import org.zoolu.sip.address.*;
import org.zoolu.sip.provider.SipParser;
import org.zoolu.sip.header.SipHeaders;
import org.zoolu.sip.header.ContactHeader;
import org.zoolu.tools.Parser;
import java.io.*;
import java.util.*;
import org.apache.log4j.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import org.zoolu.sip.header.BaseSipHeaders;
import org.zoolu.sip.utils.IpConverter;
import org.zoolu.tools.DateFormat;

/**
 * LocationServiceImpl is a simple implementation of a LocationService.
 * LocationServiceImpl allows creation and maintainance of a location service
 * for registered users.
 */
public class LocationServiceImpl implements LocationService {

    /**
     * Maximum expiration date (equivalent to NEVER).
     * <p/>
     * Note: time 3116354400000 is 2/10/2068, that is when I will be 100 years
     * old.. good luck! ;)
     */
    static Logger logger = Logger.getLogger(LocationServiceImpl.class.getName());
    static final long NEVER = (long) 31163544 * 100000;
    /**
     * LocationService name.
     */
    String file_name = null;
    /**
     * Whether the Location DB has been changed without saving.
     */
    boolean changed = false;
    /**
     * Users bindings. Set of pairs of { (String)user , (UserBindingInfo)binding
     * }.
     */
    Hashtable users;

    /**
     * Creates a new LocationServiceImpl
     */
    public LocationServiceImpl(String file_name) {
        this.file_name = file_name;
        if (file_name == null) {
            System.err.println("WARNING: no file has been provided for location DB: only temporary memory (RAM) will be used.");
        }
        users = new Hashtable();
//        load();
    }
    
    public LocationServiceImpl() {
        users = new Hashtable();
//        load();
    }

    // **************** Methods of interface Registry ****************
    /**
     * Syncronizes the database. <p> Can be used, for example, to save the
     * current memory image of the DB.
     */
    public void sync() {
        if (changed) {
            save();
        }
    }
    
    public void forceSync() {
        save();
    }

    /**
     * Returns the numbers of users in the database.
     *
     * @return the numbers of user entries
     */
    public int size() {
        return users.size();
    }

    /**
     * Returns an enumeration of the users in this database.
     *
     * @return the list of user names as an Enumeration of String
     */
    public Enumeration getUsers() {
        return users.keys();
    }
    
    public String getNewUser(String user) {
        for (Enumeration u = getUsers(); u.hasMoreElements();) {
            String userName = (String) u.nextElement();
            if (userName.startsWith(user + "@")) {
                return userName;
            }
        }
        return "";
    }

    /**
     * Whether a user is present in the database and can be used as key.
     *
     * @param user the user name
     * @return true if the user name is present as key
     */
    public boolean hasUser(String user) {
        return (users.containsKey(user));
    }

    /**
     * Adds a new user at the database.
     *
     * @param user the user name
     * @return this object
     */
    public Repository addUser(String user) {
        if (hasUser(user)) {
            return this;
        }
        UserBindingInfo ur = new UserBindingInfo(user);
        users.put(user, ur);
        changed = true;
        return this;
    }

    /**
     * Removes the user from the database.
     *
     * @param user the user name
     * @return this object
     */
    public Repository removeUser(String user) {
        if (!hasUser(user)) {
            return this;
        }
        //else
        users.remove(user);
        changed = true;
        return this;
    }

    /**
     * Removes all users from the database.
     *
     * @return this object
     */
    public Repository removeAllUsers() {
        users.clear();
        changed = true;
        return this;
    }

    /**
     * Gets the String value of this Object.
     *
     * @return the String value
     */
    public String toString() {
        String str = "";
        for (Enumeration i = getUserBindings(); i.hasMoreElements();) {
            UserBindingInfo u = (UserBindingInfo) i.nextElement();
            str += u.toString();
        }
        return str;
    }

    // **************** Methods of interface LocationService ****************
    /**
     * Whether the user has contact <i>url</i>.
     *
     * @param user the user name
     * @param url the contact URL
     * @return true if is the contact present
     */
    public boolean hasUserContact(String user, String url) {
        if (!hasUser(user)) {
            return false;
        }
        //else
        return getUserBindingInfo(user).hasContact(url);
    }

    /**
     * Adds a contact.
     *
     * @param user the user name
     * @param contact the contact NameAddress
     * @param expire the contact expire Date
     * @return this object
     */
    public LocationService addUserContact(String user, NameAddress name_addresss, Date expire) {
        if (!hasUser(user)) {
            addUser(user);
        }
        UserBindingInfo ur = getUserBindingInfo(user);
        ur.addContact(name_addresss, expire);
        try {
            String[] temp_checking_local_ip = user.split("@");
            if (!temp_checking_local_ip[1].contains(".")) {
                temp_checking_local_ip[1] = IpConverter.longToIp(Long.valueOf(temp_checking_local_ip[1]));
            }
            //---------------------------- by ashraful--------------------
            Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
            boolean local_ip_mached = false;
            for (; n.hasMoreElements();) {
                NetworkInterface e = n.nextElement();
                Enumeration<InetAddress> a = e.getInetAddresses();
                for (; a.hasMoreElements();) {
                    InetAddress addr = a.nextElement();
                    if (addr.getHostAddress().toString().equals(temp_checking_local_ip[1])) {
                        local_ip_mached = true;
                        break;
                    }
                }
            }
            //----------------------------end ashraful-------------------
            if (local_ip_mached) {
//---------------------------------- adding to mysql database (start) ----------------------------------------------                
                try {
                    addToDatabase(user, name_addresss, ur, expire);
                } catch (Exception ex) {
                    logger.debug("add to database exception-->" + ex);
                }
//---------------------------------- adding to mysql database (end) ----------------------------------------------                
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(LocationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        changed = true;
        return this;
    }
    
    private void addToDatabase(String user, NameAddress name_addresss, UserBindingInfo ur, Date expire) {

//------------------------------------------------------------------------ inserting info into database (start) -----------------------------------------------------        

        String sql = "";
        String in_user_name, in_contact_ip, in_contact_port = "", in_comment;
        int in_id;
        PreparedStatement ps = null;
        in_user_name = user.substring(0, user.indexOf('@'));
        in_contact_ip = name_addresss.getAddress().toString().substring(name_addresss.getAddress().toString().lastIndexOf('@') + 1, name_addresss.getAddress().toString().lastIndexOf(SipParser.header_colon));
        if (name_addresss.getAddress().toString().indexOf(';') > 0) {
            in_contact_port = name_addresss.getAddress().toString().substring(name_addresss.getAddress().toString().lastIndexOf(SipParser.header_colon) + 1, name_addresss.getAddress().toString().indexOf(';'));
        } else {
            in_contact_port = name_addresss.getAddress().toString().substring(name_addresss.getAddress().toString().lastIndexOf(SipParser.header_colon) + 1, name_addresss.getAddress().toString().length());
        }

        /*        int ind = 0;
         String[] temp = name_addresss.getAddress().toString().trim().split(":");
         for (ind = 0; ind < temp[2].length(); ind++) {
         try {
         Double.parseDouble(String.valueOf(temp[2].charAt(ind)));
         } catch (NumberFormatException e) {
         break;
         }
         }
         String in_full_contact = temp[0] + ":" + temp[1] + ":" + temp[2].substring(0, ind);
         in_comment = name_addresss.getAddress().toString().substring(temp[0].length() + temp[1].length() + ind + 2, name_addresss.getAddress().toString().length());
         */
        /*       int counter = 0;
         String hdr = "";
         String user_ip = "";
         String port = "";
         int ind = 0;
         StringTokenizer st = new StringTokenizer(name_addresss.getAddress().toString().trim(), SipParser.after_header);
         while (st.hasMoreTokens()) {
         counter++;
         if (counter == 1) {
         hdr = st.nextToken();
         } else if (counter == 2) {
         user_ip = st.nextToken();
         } else {
         port = st.nextToken();
         for (ind = 0; ind < port.length(); ind++) {
         try {
         Double.parseDouble(String.valueOf(port.charAt(ind)));
         } catch (NumberFormatException e) {
         break;
         }
         }
         }
         }
         String in_full_contact = hdr + SipParser.after_header + user_ip + SipParser.after_header + port.substring(0, ind);
         //in_comment = name_addresss.getAddress().toString().substring(temp[0].length() + temp[1].length() + ind + 2, name_addresss.getAddress().toString().length());
         */
        
        ResultSet rs = null;
        try {
            sql = "select id from live_users where contact = ?";
            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
            ps.setString(1, name_addresss.getAddress().toString().trim());
            rs = ps.executeQuery();
            
            
            if (rs.next()) {
                in_id = rs.getInt("id");
                sql = "update live_users set expire_time = ?,expire_time_in_timestamp=? where id = ?";
                
                ps.close();
                ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
                ps.setString(1, DateFormat.formatEEEddMMM(expire).toString());
                ps.setTimestamp(2, Timestamp.valueOf(DateFormat.getTimeStamp(expire)));
                ps.setInt(3, in_id);
                ps.executeUpdate();
            } else {
                
                sql = "insert into live_users(user_name, contact_ip, contact_port, contact, expire_time ,expire_time_in_timestamp , comment) values(?,?,?,?,?,?,?)";
                
                ps.close();
                ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
                ps.setString(1, in_user_name);
                ps.setString(2, in_contact_ip);
                ps.setString(3, in_contact_port);
                ps.setString(4, name_addresss.getAddress().toString().trim());
                ps.setString(5, DateFormat.formatEEEddMMM(expire).toString());
                ps.setTimestamp(6, Timestamp.valueOf(DateFormat.getTimeStamp(expire)));
                ps.setString(7, "");
                
                ps.executeUpdate();
                
            }
            
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(LocationServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
        }

//------------------------------------------------------------------------ inserting info into database (end) -----------------------------------------------------                

//------------------------------------------------------------------------ pushing data to other domains (start) -------------------------------------------------------

        String str = "To: " + ur.getName() + "\r\n";
        ContactHeader ch = ur.getContactHeader(name_addresss.getAddress().toString());
        if (ch.getExpiresDate().getTime() >= LocationServiceImpl.NEVER) {
            (ch = new ContactHeader(ch)).removeExpires().setParameter("expires", "\"NEVER\"");
        }
        str += ch.toString();
        Send.sendEventMsg(str, ServerProfile.remote_domains, ServerProfile.remote_port);

//------------------------------------------------------------------------ pushing data to other domains (end) -------------------------------------------------------                    

    }

    /**
     * Removes a contact.
     *
     * @param user the user name
     * @param url the contact URL
     * @return this object
     */
    public LocationService removeUserContact(String user, String url) {
        if (!hasUser(user)) {
            return this;
        }
        //else
        UserBindingInfo ur = getUserBindingInfo(user);
        ur.removeContact(url);
        changed = true;
        return this;
    }

    /**
     * Gets the user contacts that are not expired.
     *
     * @param user the user name
     * @return the list of contact URLs as Enumeration of String
     */
    public Enumeration getUserContactURLs(String user) {
        if (!hasUser(user)) {
            return null;
        }
        //else
        changed = true;
        return getUserBindingInfo(user).getContacts();
    }

    /**
     * Gets NameAddress value of the user contact.
     *
     * @param user the user name
     * @param url the contact URL
     * @return the contact NameAddress
     */
    public NameAddress getUserContactNameAddress(String user, String url) {
        if (!hasUser(user)) {
            return null;
        }
        //else
        return getUserBindingInfo(user).getNameAddress(url);
    }

    /**
     * Gets expiration date of the user contact.
     *
     * @param user the user name
     * @param url the contact URL
     * @return the contact expire Date
     */
    public Date getUserContactExpirationDate(String user, String url) {
        if (!hasUser(user)) {
            return null;
        }
        //else
        return getUserBindingInfo(user).getExpirationDate(url);
    }

    /**
     * Whether the contact is expired.
     *
     * @param user the user name
     * @param url the contact URL
     * @return true if it has expired
     */
    public boolean isUserContactExpired(String user, String url) {
        if (!hasUser(user)) {
            return true;
        }
        //else
        return getUserBindingInfo(user).isExpired(url);
    }

    /**
     * Removes all contacts from the database.
     *
     * @return this object
     */
    /*public LocationService removeAllContacts()
     {  for (Enumeration i=getUserBindings(); i.hasMoreElements(); )
     {  ((UserBindingInfo)i.nextElement()).removeContacts();
     }
     changed=true;
     return this;
     }*/
    /**
     * Adds a 'static' contact that never expires. A static contact is a sort of
     * 'alias' for the user's AOR.
     *
     * @param user the user name
     * @param name_addresss the contact NameAddress
     * @return this object
     */
    public LocationService addUserStaticContact(String user, NameAddress name_addresss) {
        return addUserContact(user, name_addresss, new Date(NEVER));
    }

    /**
     * Whether the contact is 'static', that is it never expires. A static
     * contact is a sort of 'alias' for the user's AOR.
     *
     * @param user the user name
     * @param url the contact URL
     * @return true if it static
     */
    public boolean isUserContactStatic(String user, String url) {
        return getUserContactExpirationDate(user, url).getTime() >= NEVER;
    }

    // ***************************** Private methods *****************************
    /**
     * Returns the name of the database.
     */
    private String getName() {
        return file_name;
    }

    /**
     * Whether the database is changed.
     */
    private boolean isChanged() {
        return changed;
    }

    /**
     * Adds a user record in the database
     */
    private void addUserBindingInfo(UserBindingInfo ur) {
        if (hasUser(ur.getName())) {
            removeUser(ur.getName());
        }
        users.put(ur.getName(), ur);
    }

    /**
     * Adds a user record in the database
     */
    private UserBindingInfo getUserBindingInfo(String user) {
        return (UserBindingInfo) users.get(user);
    }

    /**
     * Returns an enumeration of the values in this database
     */
    private Enumeration getUserBindings() {
        return users.elements();
    }

    /**
     * Loads the database
     */
    private void load() {
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = ServerEngine.dbConnection.connection.createStatement();
            rs = stmt.executeQuery("select * from live_users");
            
            while (rs.next()) {
                String line = "To: " + rs.getString("user_name") + "@" + rs.getString("contact_ip") + "\r\n";
                line += "Contact: <" + rs.getString("contact") + ">;expires=\"" + rs.getString("expire_time") + "\"" + "\r\n";
                
                String user = null;
                if (line == null) {
                    break;
                }
                if (line.startsWith("#")) {
                    continue;
                }
                if (line.startsWith("To")) {
                    Parser par = new Parser(line);
                    user = par.skipString().getString();
                    addUser(user);
                    continue;
                }
                
                
                if (line.startsWith(SipHeaders.Contact)) {
                    SipParser par = new SipParser(line);
                    NameAddress name_address = ((SipParser) par.skipString()).getNameAddress();
                    String expire_value = par.goTo("expires=").skipN(8).getStringUnquoted();
                    if (expire_value.equalsIgnoreCase("NEVER")) {
                        addUserStaticContact(user, name_address);
                    } else {
                        Date expire_time = (new SipParser(expire_value)).getDate();
                        addUserContact(user, name_address, expire_time);
                    }
                    Date date = getUserContactExpirationDate(user, name_address.getAddress().toString());
                    continue;
                }
            }
        } catch (Exception ex) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
            }
        }
    }

    /**
     * Saves the database
     */
    private synchronized void save() {
        if (file_name == null) {
            return;
        }
        // else
        BufferedWriter out = null;
        changed = false;
        try {
            out = new BufferedWriter(new FileWriter(file_name));
            out.write(this.toString());
            out.close();
        } catch (IOException e) {
            System.err.println("WARNING: error trying to write on file \"" + file_name + "\"");
            logger.debug("LocationServiceImpL:WARNING: error trying to write on file \"" + file_name + "\"" + e.getMessage());
            return;
        }
    }

    //------------------------------------- Loading LocationService (start) --------------------------------------    
    public void loadLocationServiceFromDB() {
        String sql = "select * from live_users";
        String user = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = ServerEngine.dbConnection.connection.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                try {
                    user = rs.getString("user_name") + "@" + ServerProfile.host_ip;
                    if (!hasUser(user)) {
                        addUser(user);
                    }
                    
                    String con = BaseSipHeaders.Contact + SipParser.header_colon + " " + SipParser.bracket_header_start + rs.getString("contact") + rs.getString("comment") + SipParser.bracket_header_end + ";expires=\"" + rs.getString("expire_time") + "\"";
                    SipParser par = new SipParser(con);
                    NameAddress name_address = ((SipParser) par.skipString()).getNameAddress();
                    
                    DateFormat dt = new DateFormat();
                    Date expire_date = dt.parseEEEddMMM(rs.getString("expire_time"), 0);
                    
                    UserBindingInfo ur = getUserBindingInfo(user);
                    ur.addContact(name_address, expire_date);
                } catch (Exception e) {
                    logger.debug("Error while loading locationService --> " + e.getMessage());
                }
            }
            
        } catch (SQLException ex) {
            logger.debug("Error while reading db to load locationService --> " + ex.getMessage());
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
        }
        
        this.forceSync();
    }
//------------------------------------- Loading LocationService (end) --------------------------------------
}

/**
 * User's binding info. This class represents a user record of the location DB.
 * <p> A UserBindingInfo contains the user name, and a set of contact
 * information (i.e. contact and expire-time). <p> Method getContacts() returns
 * an Enumeration of String values rapresenting the various contact SipURLs.
 * Such values can be used as keys for getting for each contact both the contact
 * NameAddress and the expire Date.
 */
class UserBindingInfo {

    /**
     * User name
     */
    String name;
    /**
     * Hashtable of ContactHeader with String as key.
     */
    Hashtable contact_list;
    Logger logger = Logger.getLogger(UserBindingInfo.class.getName());

    /**
     * Costructs a new UserBindingInfo for user <i>name</i>.
     *
     * @param name the user name
     */
    public UserBindingInfo(String name) {
        this.name = name;
        contact_list = new Hashtable();
    }

    /**
     * Gets the user name.
     *
     * @return the user name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the user contacts.
     *
     * @return the user contacts as an Enumeration of String
     */
    public Enumeration getContacts() {
        return contact_list.keys();
    }

    /**
     * Whether the user has any registered contact.
     *
     * @param url the contact url (String)
     * @return true if one or more contacts are present
     */
    public boolean hasContact(String url) {
        return contact_list.containsKey(url);
    }

    /**
     * Adds a new contact.
     *
     * @param contact the contact address (NameAddress)
     * @param expire the expire value (Date)
     * @return this object
     */
    public UserBindingInfo addContact(NameAddress contact, Date expire) {
        String key = contact.getAddress().toString();
        PreparedStatement ps = null;
        
        if (!contact_list.containsKey(key)) {
            contact_list.put(key, (new ContactHeader(contact)).setExpires(expire));
        }
        return this;
    }

    /**
     * Removes a contact.
     *
     * @param url the contact url (String)
     * @return this object
     */
    public UserBindingInfo removeContact(String url) {
        if (contact_list.containsKey(url)) {
            contact_list.remove(url);
        }
        return this;
    }

    /**
     * Gets NameAddress of a contact.
     *
     * @param url the contact url (String)
     * @return the contact NameAddress, or null if the contact is not present
     */
    public NameAddress getNameAddress(String url) {
        if (contact_list.containsKey(url)) {
//            logger.debug("public NameAddress getNameAddress(String url)  ---> " + ((ContactHeader) contact_list.get(url)).getNameAddress());
            return ((ContactHeader) contact_list.get(url)).getNameAddress();
        } else {
            return null;
        }
    }

    /**
     * Whether the contact is expired.
     *
     * @param url the contact url (String)
     * @return true if the contact is expired or contact does not exist
     */
    public boolean isExpired(String url) {
        if (contact_list.containsKey(url)) {
            return ((ContactHeader) contact_list.get(url)).isExpired();
        } else {
            return true;
        }
    }

    /**
     * Gets expiration date.
     *
     * @param url the contact url (String)
     * @return the expire Date
     */
    public Date getExpirationDate(String url) {
        if (contact_list.containsKey(url)) {
//            logger.debug("public Date getExpirationDate(String url)   ---> " + (((ContactHeader) contact_list.get(url)).getExpiresDate()));
            return ((ContactHeader) contact_list.get(url)).getExpiresDate();
        } else {
            return null;
        }
    }

    /**
     * Removes all contacts.
     *
     * @return this object
     */
    /*public UserBindingInfo removeContacts()
     {  contact_list.clear();
     return this;
     }*/
    /**
     * Gets the String value of this Object.
     *
     * @return the String value
     */
    public String toString() {
        String str = "To: " + name + "\r\n";
        for (Enumeration i = getContacts(); i.hasMoreElements();) {
//            logger.debug("contact_list.get(i.nextElement()) --->    " + contact_list.get(i.nextElement()));
            ContactHeader ch = (ContactHeader) contact_list.get(i.nextElement());
            if (ch.getExpiresDate().getTime() >= LocationServiceImpl.NEVER) {
                (ch = new ContactHeader(ch)).removeExpires().setParameter("expires", "\"NEVER\"");
            }
            str += ch.toString();
        }
        return str;
    }

//------------------------------------------ getting contact info for sending to remote servers (start)----------------------------------------    
    ContactHeader getContactHeader(String url) {
        return ((ContactHeader) contact_list.get(url));
    }
//------------------------------------------ getting contact info for sending to remote servers (end)----------------------------------------        
}
