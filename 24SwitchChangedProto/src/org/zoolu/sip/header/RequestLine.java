/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package org.zoolu.sip.header;

import org.zoolu.sip.address.*;
import org.zoolu.sip.provider.SipParser;
import org.zoolu.sip.utils.IpConverter;

/**
 * SIP Request-line, i.e. the first line of a request message <BR> The initial
 * Request-URI of the message SHOULD be set to the value of the URI in the To
 * field.
 */
public class RequestLine {

    /**
     * Request method
     */
    protected String method;
    /**
     * Request target
     */
    protected SipURL url;

    /**
     * Creates a new RequestLine <i>request</i> with <i>sipurl</i> as recipient.
     */
    public RequestLine(String request, String sipUrl) {
       /* 
        String[] temp = sipUrl.split("|");
        String ip = "";
        if (temp[1].contains("@")) {
            String[] temp1 = temp[1].split("@");
            ip = temp1[0] + SipParser.after_header + IpConverter.lognToIp(Long.parseLong(temp1[1]));
        } else {
            ip = IpConverter.lognToIp(Long.parseLong(temp[1]));
        }
        url = new SipURL(temp[0] + SipParser.after_header + ip);
        */
      method=request;
      url=new SipURL(sipUrl);
//      url.changeLongToIp();
    }

    /**
     * Creates a new RequestLine <i>request</i> with <i>sipurl</i> as recipient.
     */
    public RequestLine(String request, SipURL sipUrl) {
        method = request;
        url = sipUrl;
    }

    /**
     * Creates a new copy of the RequestLine.
     */
    public Object clone() {
        return new RequestLine(getMethod(), getAddress());
    }

    /**
     * Whether Object <i>obj</i> is "equal to" this RequestLine.
     */
    public boolean equals(Object obj) {  //if (o.getClass().getSuperclass()!=this.getClass().getSuperclass()) return false;
        try {
            RequestLine r = (RequestLine) obj;
            if (r.getMethod().equals(this.getMethod()) && r.getAddress().equals(this.getAddress())) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Gets String value of this Object.
     */
    public String toString() {
        return method + " " + url + " 24FNF/1.5  ";
    }

    /**
     * Gets the request method.
     */
    public String getMethod() {
        return method;
    }

    /**
     * Gets the request target.
     */
    public SipURL getAddress() {
        return url;
    }
}
