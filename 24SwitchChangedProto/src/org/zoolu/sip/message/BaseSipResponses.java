/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package org.zoolu.sip.message;

import static org.zoolu.sip.message.BaseSipResponses.reasons;

/** Class SipResponses provides all raeson-phrases
 * corrspondent to the various SIP response codes */
public abstract class BaseSipResponses {
    //static Hashtable reasons;

    protected static String[] reasons;
    private static boolean is_init = false;

    protected static void init() {
        if (is_init) {
            return;
        }
        //else

        //reasons=new Hashtable();
        //reasons.put(new Integer(100),"Trying");
        //..
        reasons = new String[1700];
        for (int i = 1000; i < 1700; i++) {
            reasons[i] = null;
        }

        // Not defined (included just to robustness)
        reasons[1000] = "Internal error"; //"Ovvontorin somossa"; 

        // Informational
        reasons[1100] = "Trying"; //"Chesta";
        reasons[1180] = "Ringing"; //"Dhonito";
        reasons[1181] = "Call Is Being Forwarded"; //"Barta Ogroshor kora hoyese";
        reasons[1182] = "Queued"; //"Shariboddho";
        reasons[1183] = "Session Progress"; //"Shomoy Ogrogoti";

        // Success
        reasons[1200] = "OK"; //"Assa";

        // Redirection
        reasons[1300] = "Multiple Choices"; //"Ekadhik Posondo";
        reasons[1301] = "Moved Permanently"; //"Shorano Sthayivabe";
        reasons[1302] = "Moved Temporarily"; //"Shorano Shamoikvabe";
        reasons[1305] = "Use Proxy"; //"Bebohar Protinidhi";
        reasons[1380] = "Alternative Service"; //"Bikolpo Sheba";

        // Client-Error
        reasons[1400] = "Bad Request"; //"Ku Prostab";
        reasons[1401] = "Unauthorized"; //"Oboidho";
        reasons[1402] = "Payment Required"; //"Porisodh Korun";
        reasons[1403] = "Forbidden"; //"Nisiddho";
        reasons[1404] = "Not Found"; //"Pai Nai";
        reasons[1405] = "Method Not Allowed"; //"Poddhoti Boidho Noy";
        reasons[1406] = "Not Acceptable"; //"Grohonjoggo Noy";
        reasons[1407] = "Proxy Authentication Required"; //"Protinidhi Proman Abosshok";
        reasons[1408] = "Request Timeout"; //"Onurodh Somoysesh";
        reasons[1410] = "Gone"; //"Biday";
        reasons[1413] = "Request Entity Too Large"; //"Onurodh Ostitto Onek Boro";
        reasons[1414] = "Request-URI Too Large"; //"Onurodh-URI Onek Boro";
        reasons[1415] = "Unsupported Media Type"; //"Oshomorthito Media Nomuna";
        reasons[1416] = "Unsupported URI Scheme"; //"Oshomorthito URI Prokolpo";
        reasons[1420] = "Bad Extension"; //"Kharap Proshar";
        reasons[1421] = "Extension Required"; //"Proshar Dorkar";
        reasons[1423] = "Interval Too Brief"; //"Biroti Khub Kom";
        reasons[1480] = "Temporarily not available";//"Shamoikvabe prappo noy";
        reasons[1481] = "Call Leg/Transaction Does Not Exist"; //"Ahoban Pa/Lenden Somvob Noy";
        reasons[1482] = "Loop Detected"; //"Ghuron Shonakto";
        reasons[1483] = "Too Many Hops"; //"Onek Besi Hops";
        reasons[1484] = "Address Incomplete"; //"Thikana Oshompurno";
        reasons[1485] = "Ambiguous"; //"Osposhto";
        reasons[1486] = "Busy Here"; //"Besto Ekhane";
        reasons[1487] = "Request Terminated"; //"Onurodh Batil";
        reasons[1488] = "Not Acceptable Here"; //"Grohonjoggo Noy";
        reasons[1491] = "Request Pending"; //"Onurodh Multobi";
        reasons[1493] = "Undecipherable"; //"Durboddho";

        // Server-Error
        reasons[1500] = "Internal Server Error"; //"Ovvontorin Server Truti";
        reasons[1501] = "Not Implemented"; //"Proyog Hoyni";
        reasons[1502] = "Bad Gateway"; //"Kharap Dorja";
        reasons[1503] = "Service Unavailable"; //"Sheba Nei";
        reasons[1504] = "Server Time-out"; //"Server Somoy-sesh";
        reasons[1505] = "SIP Version not supported"; //"24FNF Version Shomorthon kore na";
        reasons[1513] = "Message Too Large"; //"Barta Onek Bishal";

        // Global-Failure
        reasons[1600] = "Busy Everywhere"; //"Besto Charidike";
        reasons[1603] = "Decline"; //"Ossikar";
        reasons[1604] = "Does not exist anywhere"; //"Kothao kono ostitto nei";
        reasons[1606] = "Not Acceptable";//"Grohonjoggo Noy";

        is_init = true;
    }

    /** Gets the reason phrase of a given response <i>code</i> */
    public static String reasonOf(int code) {
        if (!is_init) {
            init();
        }
        if (reasons[code] != null) {
            return reasons[code];
        } else {
            return reasons[((int) (code / 1000)) * 1000];
        }
    }
    /** Sets the reason phrase for a given response <i>code</i> */
    /*public static void setReason(int code, String reason)
    {  reasons[((int)(code/100))*100]=reason;
    }*/
}