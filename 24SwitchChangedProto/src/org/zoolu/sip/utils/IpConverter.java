/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zoolu.sip.utils;

import header.SIPtoFnF2;
import java.security.DigestException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.StringTokenizer;
import org.zoolu.sip.address.SipURL;
import org.zoolu.sip.message.BaseMessage;
import org.zoolu.sip.provider.SipParser;

/**
 *
 * @author reefat
 */
public class IpConverter {

    public static long ipToLong(String ipStr) {
        String[] ip = ipStr.split("\\.");
        long ipVal = 0;
        for (int i = 0; i < 4; i++) {
            ipVal <<= 8;
            ipVal |= Integer.parseInt(ip[i]);
        }
        ipVal -= 2147483647;
        return ipVal;
    }

    public static String longToIp(long l) {
        
        l += 2147483647;
        return ((l >> 24) & 0xFF) + "."
                + ((l >> 16) & 0xFF) + "."
                + ((l >> 8) & 0xFF) + "."
                + (l & 0xFF);
    }

    public static String convertUrlIpToLong(String url) {
        int counter = 0;
        String hdr = "";
        String user_ip = "";
        String port = "";
        StringTokenizer st = new StringTokenizer(url.toString(), SipParser.header_colon);
        while (st.hasMoreTokens()) {
            counter++;
            if (counter == 1) {
                hdr = st.nextToken();
            } else if (counter == 2) {
                user_ip = st.nextToken();
                if (user_ip.contains("@")) {
                    String temp[] = user_ip.split("@");
                    user_ip = temp[0] + "@" + String.valueOf(ipToLong(temp[1]));
                } else {
                    user_ip = String.valueOf(ipToLong(st.nextToken()));
                }
            } else {
                port = st.nextToken();
            }
        }
        url = hdr + SipParser.header_colon + user_ip + SipParser.header_colon + port;
        return url;
    }
    static String sip200 = "SIP/2.0 200 OK\n"
            + "CSeq: 1 INVITE\n"
            + "Via: SIP/2.0/UDP 38.108.92.141:5080;branch=z9hG4bKe079993741d75b1246a5c62c\n"
            + "Via: SIP/2.0/UDP 192.168.1.95:5072;branch=z9hG4bK462d885e\n"
            + "From: \"reefat\" <sip:reefat@38.108.92.141>;tag=469109908997\n"
            + "Call-ID: 956796366044@192.168.1.95\n"
            + "To: <sip:+8801911722319@38.108.92.141>;tag=19053513032330250972755821\n"
            + "Contact: <sip:38.108.92.156:5060;transport=udp>\n"
            + "Content-Type: application/sdp\n"
            + "Content-Length: 248\n"
            + "Record-Route: <sip:38.108.92.141:5080;lr>\n"
            + "\n"
            + "v=0\n"
            + "o=VoipSwitch 6820 6820 IN IP4 38.108.92.156\n"
            + "s=VoipSIP\n"
            + "i=Audio Session\n"
            + "c=IN IP4 38.108.92.156\n"
            + "t=0 0\n"
            + "m=audio 5820 RTP/AVP 18 101\n"
            + "a=rtpmap:18 G729/8000/1\n"
            + "a=fmtp:18 annexb=no\n"
            + "a=rtpmap:101 telephone-event/8000\n"
            + "a=fmtp:101 0-15\n"
            + "a=sendrecv";

    	public static String toHex(byte[] bsData) {
		int nDataLen = bsData.length;
		String sHex = "";
		for (int nIter = 0; nIter < nDataLen; nIter++) {
			int nValue = (bsData[nIter] + 256) % 256;
			int nIndex1 = nValue >> 4;
			sHex += Integer.toHexString(nIndex1);
			int nIndex2 = nValue & 0x0f;
			sHex += Integer.toHexString(nIndex2);
		}
		return sHex;
	}
    public static void main(String[] args) throws NoSuchAlgorithmException, DigestException {
        System.out.println("getIPVal ==>  " + ipToLong("192.168.1.95"));
       System.out.println("IP-->" + longToIp(Long.valueOf("1593944256")));
        
        //System.out.println("IP-->" + longToIp(Long.valueOf("1084752224")));
        System.out.println("Current Time-->"+System.currentTimeMillis());
        
        int nDigestLen = 16;
	byte[] messageDigest = new byte[nDigestLen];
        byte[] plainText1 = "anwar".getBytes();
        byte[] plainText2 = "reefat".getBytes();
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(plainText1, 0, plainText1.length);
        md.update(plainText2, 0, plainText2.length);
	md.digest(messageDigest, 0, nDigestLen);
        System.out.println("MD5--->"+toHex(messageDigest));
        //System.out.println("hudai ==>  " + longToIp(ipToLong("192.168.1.100")));

        //String delims = BaseMessage.SIP_VERSION;

        //StringTokenizer st = new StringTokenizer("1654|sadad|asdsd", delims);
        //System.out.println("No of Token = " + st.countTokens());

        //while (st.hasMoreTokens()) {
        //    System.out.println(st.nextToken());
        // }
        // System.out.println("new SIPtoFnF2().changeToFnF(sip200)"+new SIPtoFnF2().changeToFnF(sip200));
//        String via = "SIP/2.0/UDP 38.108.92.141:5080;branch=z9hG4bKcd536a8d33418d8730cb38f4";
//        System.out.println("-->" + via.substring(via.indexOf(" ") + 1, via.indexOf(":")));
//        String bas = "SIP/2.0 1200 Assa";
        //System.out.println(bas.substring(bas.indexOf("1"-1), bas.indexOf(" ")));

    }
}
