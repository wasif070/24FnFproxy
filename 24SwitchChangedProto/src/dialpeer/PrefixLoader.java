/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dialpeer;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;
import org.zoolu.sip.utils.AppConstants;

/**
 *
 * @author Rajib
 */
public class PrefixLoader {

    static Logger logger = Logger.getLogger(PrefixLoader.class.getName());
    static PrefixLoader loader = null;
    private HashMap<String, String> prefixMap = null;
    private long loadingTime = 0;

    public PrefixLoader() {
        forceReload();
    }

    public static PrefixLoader getInstance() {
        if (loader == null) {
            createLoader();
        }
        return loader;
    }

    private synchronized static void createLoader() {
        if (loader == null) {
            loader = new PrefixLoader();
        }
    }

    private void reload() {
        prefixMap = new HashMap<String, String>();
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select prefix,country_code from prefix_map";
            //logger.debug("Prefix sql-->" + sql);
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                prefixMap.put(rs.getString("country_code"), rs.getString("prefix"));
            }
        } catch (Exception e) {
            logger.debug("Exception in PrefixLoader-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > AppConstants.LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized String getPrefix(String phone_no) {
        checkForReload();
        String prefix = "";
        byte[] bytes = phone_no.getBytes();
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            sb.append((char) b);
            String country_code = sb.toString();
            if (prefixMap.get(country_code) != null) {
                prefix = prefixMap.get(country_code);
            }
        }
        return prefix;
    }
}
