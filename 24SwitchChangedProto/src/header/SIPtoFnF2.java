/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package header;

import local.server.ServerEngine;
import org.zoolu.sip.header.BaseSipHeaders;
import org.zoolu.sip.header.EndPointHeader;
import org.zoolu.sip.header.SipHeaders;
import org.zoolu.sip.header.SubscriptionStateHeader;
import org.zoolu.sip.header.ViaHeader;
import org.zoolu.sip.message.BaseMessage;
import org.zoolu.sip.message.BaseSipMethods;
import org.zoolu.sip.message.SipMethods;
import org.zoolu.sip.provider.SipParser;
import org.zoolu.sip.provider.SipProvider;
import org.zoolu.sip.utils.IpConverter;

/**
 *
 * @author Wasif Islam
 */
public class SIPtoFnF2 {

    public static String recv_ip;
    
    public static String changeToFnF(String msg, String switchIP, String routeIP, String userIP, String mediaIP) {
        String msgFnF = msg
                .replaceAll(switchIP, String.valueOf(IpConverter.ipToLong(switchIP)))
                .replaceAll(routeIP, String.valueOf(IpConverter.ipToLong(routeIP)))
                .replaceAll(userIP, String.valueOf(IpConverter.ipToLong(userIP)))
                .replaceAll(mediaIP, String.valueOf(IpConverter.ipToLong(mediaIP)))
                .replaceAll(mediaIP, String.valueOf(IpConverter.ipToLong(recv_ip)))
                .replaceAll("REGISTER", BaseSipMethods.REGISTER)
                .replaceAll("INVITE", BaseSipMethods.INVITE)
                .replaceAll("ACK", BaseSipMethods.ACK)
                .replaceAll("CANCEL", BaseSipMethods.CANCEL)
                .replaceAll("BYE", BaseSipMethods.BYE)
                .replaceAll("INFO", BaseSipMethods.INFO)
                .replaceAll("OPTIONS", BaseSipMethods.OPTIONS)
                .replaceAll("UPDATE", BaseSipMethods.UPDATE)
                .replaceAll("SUBSCRIBE", SipMethods.SUBSCRIBE)
                .replaceAll("NOTIFY", SipMethods.NOTIFY)
                .replaceAll("MESSAGE", SipMethods.MESSAGE)
                .replaceAll("PUBLISH", SipMethods.PUBLISH)
                .replaceAll("udp", BaseMessage.PROTO_UDP)
                .replaceAll("UDP", SipProvider.PROTO_UDP.toUpperCase())
                .replaceAll("tcp", BaseMessage.PROTO_TCP)
                .replaceAll("tls", BaseMessage.PROTO_TLS)
                .replaceAll("sctp", BaseMessage.PROTO_SCTP)
                .replaceAll("SIP/", BaseMessage.SIP_VERSION)
                .replaceAll("/2.0", "/1.5")
                .replaceAll("Refer-To", SipHeaders.Refer_To)
                .replaceAll("Referred-By", SipHeaders.Referred_By)
                .replaceAll("Replaces", SipHeaders.Replaces)
                .replaceAll("Event", SipHeaders.Event)
                .replaceAll("Allow-Events", SipHeaders.Allow_Events)
                .replaceAll("Subscription-State", "SipHeaders.Subscription_State")
                .replaceAll("Accept", BaseSipHeaders.Accept)
                .replaceAll("Alert-Info", BaseSipHeaders.Alert_Info)
                .replaceAll("Allow", BaseSipHeaders.Allow)
                .replaceAll("Authentication-Info", BaseSipHeaders.Authentication_Info)
                .replaceAll("Authorization", BaseSipHeaders.Authorization)
                .replaceAll("Call-ID", BaseSipHeaders.Call_ID)
                .replaceAll("Contact", BaseSipHeaders.Contact)
                .replaceAll("Content-Length", BaseSipHeaders.Content_Length)
                .replaceAll("Content-Type", BaseSipHeaders.Content_Type)
                .replaceAll("CSeq", BaseSipHeaders.CSeq)
                .replaceAll("Date", BaseSipHeaders.Date)
                .replaceAll("Expires", BaseSipHeaders.Expires)
                .replaceAll("From", BaseSipHeaders.From)
                .replaceAll("User-Agent", BaseSipHeaders.User_Agent)
                .replaceAll("Max-Forwards", BaseSipHeaders.Max_Forwards)
                .replaceAll("Proxy-Authenticate", BaseSipHeaders.Proxy_Authenticate)
                .replaceAll("Proxy-Authorization", BaseSipHeaders.Proxy_Authorization)
                .replaceAll("Proxy-Require", BaseSipHeaders.Proxy_Require)
                .replaceAll("Record-Route", BaseSipHeaders.Record_Route)
                .replaceAll("Require", BaseSipHeaders.Require)
                .replaceAll("Route", BaseSipHeaders.Route)
                .replaceAll("Server", BaseSipHeaders.Server)
                .replaceAll("Subject", BaseSipHeaders.Subject)
                .replaceAll("Supported", BaseSipHeaders.Supported)
                .replaceAll("To", BaseSipHeaders.To)
                .replaceAll("Unsupported", BaseSipHeaders.Unsupported)
                .replaceAll("Via", BaseSipHeaders.Via)
                .replaceAll("WWW-Authenticate", BaseSipHeaders.WWW_Authenticate)
                .replaceAll("received", ViaHeader.received_param)
                .replaceAll("rport", ViaHeader.rport_param)
                .replaceAll("branch", ViaHeader.branch_param)
                .replaceAll("maddr", ViaHeader.maddr_param)
                .replaceAll("ttl", ViaHeader.ttl_param)
                .replaceAll("tag", EndPointHeader.tag_param)
                .replaceAll("expires", EndPointHeader.expires_param)
                .replaceAll("Loop-Tag", ServerEngine.Loop_Tag)
                .replaceAll("active", SubscriptionStateHeader.ACTIVE)
                .replaceAll("pending", SubscriptionStateHeader.PENDING)
                .replaceAll("terminated", SubscriptionStateHeader.TERMINATED)
                .replaceAll("sip", SipParser.header_param)
                .replaceAll("sips", SipParser.headers_params)
                .replaceAll(":", "\\" + SipParser.header_colon)
                .replaceAll("<", "\\" + SipParser.bracket_header_start)
                .replaceAll(">", "\\" + SipParser.bracket_header_end)
                .replaceAll("\r\n", "  ")
                //.replaceAll(" 0 ", " 1000 ")
                .replaceAll(" 100 ", " 1100 ")
                .replaceAll(" 180 ", " 1180 ")
                .replaceAll(" 181 ", " 1181 ")
                .replaceAll(" 182 ", " 1182 ")
                .replaceAll(" 183 ", " 1183 ")
                .replaceAll(" 200 ", " 1200 ")
                .replaceAll(" 300 ", " 1300 ")
                .replaceAll(" 301 ", " 1301 ")
                .replaceAll(" 302 ", " 1302 ")
                .replaceAll(" 305 ", " 1305 ")
                .replaceAll(" 380 ", " 1380 ")
                .replaceAll(" 400 ", " 1400 ")
                .replaceAll(" 401 ", " 1401 ")
                .replaceAll(" 402 ", " 1402 ")
                .replaceAll(" 403 ", " 1403 ")
                .replaceAll(" 404 ", " 1404 ")
                .replaceAll(" 405 ", " 1405 ")
                .replaceAll(" 406 ", " 1406 ")
                .replaceAll(" 407 ", " 1407 ")
                .replaceAll(" 408 ", " 1408 ")
                .replaceAll(" 410 ", " 1410 ")
                .replaceAll(" 413 ", " 1413 ")
                .replaceAll(" 414 ", " 1414 ")
                .replaceAll(" 415 ", " 1415 ")
                .replaceAll(" 416 ", " 1416 ")
                .replaceAll(" 420 ", " 1420 ")
                .replaceAll(" 421 ", " 1421 ")
                .replaceAll(" 423 ", " 1423 ")
                .replaceAll(" 480 ", " 1480 ")
                .replaceAll(" 481 ", " 1481 ")
                .replaceAll(" 482 ", " 1482 ")
                .replaceAll(" 483 ", " 1483 ")
                .replaceAll(" 484 ", " 1484 ")
                .replaceAll(" 485 ", " 1485 ")
                .replaceAll(" 486 ", " 1486 ")
                .replaceAll(" 487 ", " 1487 ")
                .replaceAll(" 488 ", " 1488 ")
                .replaceAll(" 491 ", " 1491 ")
                .replaceAll(" 493 ", " 1493 ")
                .replaceAll(" 500 ", " 1500 ")
                .replaceAll(" 501 ", " 1501 ")
                .replaceAll(" 502 ", " 1502 ")
                .replaceAll(" 503 ", " 1503 ")
                .replaceAll(" 504 ", " 1504 ")
                .replaceAll(" 505 ", " 1505 ")
                .replaceAll(" 513 ", " 1513 ")
                .replaceAll(" 600 ", " 1600 ")
                .replaceAll(" 603 ", " 1603 ")
                .replaceAll(" 604 ", " 1604 ")
                .replaceAll(" 606 ", " 1606 ")
                .replaceAll(" 202 ", " 1202 ")
                .replaceAll(" 489 ", " 1489 ")
                .replaceAll("Internal error", "Ovvontorin somossa")
                .replaceAll("Trying", "Chesta")
                .replaceAll("Ringing", "Dhonito")
                .replaceAll("Call Is Being Forwarded", "Barta Ogroshor kora hoyese")
                .replaceAll("Queued", "Shariboddho")
                .replaceAll("Session Progress", "Shomoy Ogrogoti")
                .replaceAll("OK", "Assa")
                .replaceAll("Multiple Choices", "Ekadhik Posondo")
                .replaceAll("Moved Permanently", "Shorano Sthayivabe")
                .replaceAll("Moved Temporarily", "Shorano Shamoikvabe")
                .replaceAll("Use Proxy", "Bebohar Protinidhi")
                .replaceAll("Alternative Service", "Bikolpo Sheba")
                .replaceAll("Bad Request", "Ku Prostab")
                .replaceAll("Unauthorized", "Oboidho")
                .replaceAll("Payment Required", "Porisodh Korun")
                .replaceAll("Forbidden", "Nisiddho")
                .replaceAll("Not Found", "Pai Nai")
                .replaceAll("Method Not Allowed", "Poddhoti Boidho Noy")
                .replaceAll("Not Acceptable", "Grohonjoggo Noy")
                .replaceAll("Proxy Authentication Required", "Protinidhi Proman Abosshok")
                .replaceAll("Request Timeout", "Onurodh Somoysesh")
                .replaceAll("Gone", "Biday")
                .replaceAll("Request Entity Too Large", "Onurodh Ostitto Onek Boro")
                .replaceAll("Request-URI Too Large", "Onurodh-URI Onek Boro")
                .replaceAll("Unsupported Media Type", "Oshomorthito Media Nomuna")
                .replaceAll("Unsupported URI Scheme", "Oshomorthito URI Prokolpo")
                .replaceAll("Bad Extension", "Kharap Proshar")
                .replaceAll("Extension Required", "Proshar Dorkar")
                .replaceAll("Interval Too Brief", "Biroti Khub Kom")
                .replaceAll("Temporarily not available", "Shamoikvabe prappo noy")
                .replaceAll("Call Leg/Transaction Does Not Exist", "Ahoban Pa/Lenden Somvob Noy")
                .replaceAll("Loop Detected", "Ghuron Shonakto")
                .replaceAll("Too Many Hops", "Onek Besi Hops")
                .replaceAll("Address Incomplete", "Thikana Oshompurno")
                .replaceAll("Ambiguous", "Osposhto")
                .replaceAll("Busy Here", "Besto Ekhane")
                .replaceAll("Request Terminated", "Onurodh Batil")
                .replaceAll("Not Acceptable Here", "Grohonjoggo Noy")
                .replaceAll("Request Pending", "Onurodh Multobi")
                .replaceAll("Undecipherable", "Durboddho")
                .replaceAll("Internal Server Error", "Ovvontorin Server Truti")
                .replaceAll("Not Implemented", "Proyog Hoyni")
                .replaceAll("Bad Gateway", "Kharap Dorja")
                .replaceAll("Service Unavailable", "Sheba Nei")
                .replaceAll("Server Time-out", "Server Somoy-sesh")
                .replaceAll("SIP Version not supported", "24FNF Version Shomorthon kore na")
                .replaceAll("Message Too Large", "Barta Onek Bishal")
                .replaceAll("Busy Everywhere", "Besto Charidike")
                .replaceAll("Decline", "Ossikar")
                .replaceAll("Does not exist anywhere", "Kothao kono ostitto nei")
                .replaceAll("Not Acceptable", "Grohonjoggo Noy")
                .replaceAll("Accepted", "Shommoto")
                .replaceAll("Bad Event", "Kharap Ghotona");
        return msgFnF;
    }

   /* public static void main(String[] args) {
        String ss = "SIP/2.0 407 Proxy Authentication Required\n"
                + "CSeq: 1 INVITE\n"
                + "Via: SIP/2.0/UDP 38.108.92.141:5080;branch=z9hG4bKed630b1b14b2aa2dd4b9d580\n"
                + "Via: SIP/2.0/UDP 192.168.1.113:5072;branch=z9hG4bKcc1aed14\n"
                + "From: \"nazmul\" <sip:nazmul@38.108.92.141>;tag=407179432716\n"
                + "Call-ID: 017366404479@192.168.1.113\n"
                + "To: <sip:+8801913367007@38.108.92.141>;tag=26052013052136361940175905\n"
                + "Contact: <sip:38.108.92.156:5060;transport=udp>\n"
                + "Proxy-Authenticate: DIGEST realm=\"38.108.92.156\", nonce=\"136956002126050514605202121020\"\n"
                + "Content-Length: 0\n"
                + "Record-Route: <sip:38.108.92.141:5080;lr>";
    }
    */
    
}
