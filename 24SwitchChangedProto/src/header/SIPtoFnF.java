/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package header;

import static header.FnFtoSIP.parsed_switchLongIP;
import local.server.ServerEngine;
import org.zoolu.sip.header.BaseSipHeaders;
import org.zoolu.sip.header.EndPointHeader;
import org.zoolu.sip.header.ViaHeader;
import org.zoolu.sip.message.BaseMessage;
import org.zoolu.sip.message.BaseSipMethods;
import org.zoolu.sip.message.SipMethods;
import org.zoolu.sip.provider.SipParser;
import org.zoolu.sip.provider.SipProvider;
import org.zoolu.sip.utils.IpConverter;

/**
 *
 * @author Wasif Islam
 */
public class SIPtoFnF {

    //public static String recv_ip = "103.14.27.126";
    public static String recv_ip;
    static String parsed_switchIP;
    static String parsed_routeIP;
    static String parsed_userIP;

    public static String changeToFnF(String msg, String switchIP, String routeIP, String userIP, String mediaIP) {
        System.out.println("------------------------------- Inside changeToFnF() --------------------- ");
        String msgFnF = "";
        while (msg.length() > 0) {
            int nwline_index = msg.indexOf("\r\n");
            if (nwline_index == -1) {
                break;
            }
            String line = msg.substring(0, nwline_index);

            msg = msg.substring(nwline_index + 2, msg.length());

            try {
                parsed_switchIP = String.valueOf(IpConverter.ipToLong(switchIP));
            } catch (Exception e) {
                parsed_switchIP = switchIP;
            }
            try {
                parsed_routeIP = String.valueOf(IpConverter.ipToLong(routeIP));
            } catch (Exception e) {
                parsed_routeIP = routeIP;
            }
            try {
                parsed_userIP = String.valueOf(IpConverter.ipToLong(userIP));
            } catch (Exception e) {
                parsed_userIP = userIP;
            }


            if (line.startsWith("REGISTER") || line.startsWith("INVITE") || line.startsWith("ACK") || line.startsWith("CANCEL") || line.startsWith("BYE") || line.startsWith("INFO") || line.startsWith("OPTIONS") || line.startsWith("UPDATE")) {
                msgFnF += line.replace("REGISTER", BaseSipMethods.REGISTER)
                        .replace("INVITE", BaseSipMethods.INVITE)
                        .replace("ACK", BaseSipMethods.ACK)
                        .replace("CANCEL", BaseSipMethods.CANCEL)
                        .replace("BYE", BaseSipMethods.BYE)
                        .replace("INFO", BaseSipMethods.INFO)
                        .replace("OPTIONS", BaseSipMethods.OPTIONS)
                        .replace("UPDATE", BaseSipMethods.UPDATE)
                        .replace("sip", SipParser.header_param)
                        .replace(":", SipParser.header_colon)
                        .replace("SIP/", BaseMessage.SIP_VERSION)
                        .replace("/2.0", "/1.5")
                        .replace(switchIP, parsed_switchIP)
                        .replace(routeIP, parsed_routeIP)
                        .replace(userIP, parsed_userIP)
                        + "  ";
            } else if (line.startsWith("SIP/")) {
                int i = 0;
                String nw_ln = "";
                while (line.length() > 0) {
                    i++;
                    String word;
                    int index = line.indexOf(" ");
                    if (i < 3) {
                        word = line.substring(0, index);
                        line = line.substring(index + 1, line.length());
                    } else {
                        word = line.substring(0, line.length());
                        line = "";
                    }
                    switch (i) {
                        case 1:
                            nw_ln = BaseMessage.SIP_VERSION + "1.5 ";
                            break;
                        case 2:
                            try {
                                int err_code = Integer.parseInt(word);
                                nw_ln += String.valueOf(err_code + 1000) + " ";
                                // is an integer!
                            } catch (NumberFormatException e) {
                                // not an integer!
                            }
                            break;
                        default:
                            nw_ln += word;
                            /* if (word.equals("Internal error")) {
                             nw_ln += "Ovvontorin somossa";
                             } else if (word.equals("Trying")) {
                             nw_ln += "Chesta";
                             } else if (word.equals("Ringing")) {
                             nw_ln += "Dhonito";
                             } else if (word.equals("Call Is Being Forwarded")) {
                             nw_ln += "Barta Ogroshor kora hoyese";
                             } else if (word.equals("Queued")) {
                             nw_ln += "Shariboddho";
                             } else if (word.equals("Session Progress")) {
                             nw_ln += "Shomoy Ogrogoti";
                             } else if (word.equals("OK")) {
                             nw_ln += "Assa";
                             } else if (word.equals("Multiple Choices")) {
                             nw_ln += "Ekadhik Posondo";
                             } else if (word.equals("Moved Permanently")) {
                             nw_ln += "Shorano Sthayivabe";
                             } else if (word.equals("Moved Temporarily")) {
                             nw_ln += "Shorano Shamoikvabe";
                             } else if (word.equals("Use Proxy")) {
                             nw_ln += "Bebohar Protinidhi";
                             } else if (word.equals("Alternative Service")) {
                             nw_ln += "Bikolpo Sheba";
                             } else if (word.equals("Bad Request")) {
                             nw_ln += "Ku Prostab";
                             } else if (word.equals("Unauthorized")) {
                             nw_ln += "Oboidho";
                             } else if (word.equals("Payment Required")) {
                             nw_ln += "Porisodh Korun";
                             } else if (word.equals("Forbidden")) {
                             nw_ln += "Nisiddho";
                             } else if (word.equals("Not Found")) {
                             nw_ln += "Pai Nai";
                             } else if (word.equals("Method Not Allowed")) {
                             nw_ln += "Poddhoti Boidho Noy";
                             } else if (word.equals("Not Acceptable")) {
                             nw_ln += "Grohonjoggo Noy";
                             } else if (word.equals("Proxy Authentication Required")) {
                             nw_ln += "Protinidhi Proman Abosshok";
                             } else if (word.equals("Request Timeout")) {
                             nw_ln += "Onurodh Somoysesh";
                             } else if (word.equals("Proxy Authentication Required")) {
                             nw_ln += "Biday";
                             } else if (word.equals("Proxy Authentication Required")) {
                             nw_ln += "Onurodh Ostitto Onek Boro";
                             } else if (word.equals("Request-URI Too Large")) {
                             nw_ln += "Onurodh-URI Onek Boro";
                             } else if (word.equals("Unsupported Media Type")) {
                             nw_ln += "Oshomorthito Media Nomuna";
                             } else if (word.equals("Proxy Authentication Required")) {
                             nw_ln += "Oshomorthito URI Prokolpo";
                             } else if (word.equals("Unsupported URI Scheme")) {
                             nw_ln += "Protinidhi Proman Abosshok";
                             } else if (word.equals("Bad Extension")) {
                             nw_ln += "Kharap Proshar";
                             } else if (word.equals("Extension Required")) {
                             nw_ln += "Proshar Dorkar";
                             } else if (word.equals("Interval Too Brief")) {
                             nw_ln += "Biroti Khub Kom";
                             } else if (word.equals("Temporarily not available")) {
                             nw_ln += "Shamoikvabe prappo noy";
                             } else if (word.equals("Call Leg/Transaction Does Not Exist")) {
                             nw_ln += "Ahoban Pa/Lenden Somvob Noy";
                             } else if (word.equals("Loop Detected")) {
                             nw_ln += "Ghuron Shonakto";
                             } else if (word.equals("Too Many Hops")) {
                             nw_ln += "Onek Besi Hops";
                             } else if (word.equals("Address Incomplete")) {
                             nw_ln += "Thikana Oshompurno";
                             } else if (word.equals("Ambiguous")) {
                             nw_ln += "Osposhto";
                             } else if (word.equals("Busy Here")) {
                             nw_ln += "Besto Ekhane";
                             } else if (word.equals("Request Terminated")) {
                             nw_ln += "Onurodh Batil";
                             } else if (word.equals("Not Acceptable Here")) {
                             nw_ln += "Grohonjoggo Noy";
                             } else if (word.equals("Request Pending")) {
                             nw_ln += "Onurodh Multobi";
                             } else if (word.equals("Undecipherable")) {
                             nw_ln += "Durboddho";
                             } else if (word.equals("Request Pending")) {
                             nw_ln += "Onurodh Multobi";
                             } else if (word.equals("Internal Server Error")) {
                             nw_ln += "Ovvontorin Server Truti";
                             } else if (word.equals("Not Implemented")) {
                             nw_ln += "Proyog Hoyni";
                             } else if (word.equals("Bad Gateway")) {
                             nw_ln += "Kharap Dorja";
                             } else if (word.equals("Service Unavailable")) {
                             nw_ln += "Sheba Nei";
                             } else if (word.equals("Server Time-out")) {
                             nw_ln += "Server Somoy-sesh";
                             } else if (word.equals("SIP Version not supported")) {
                             nw_ln += "24FNF Version Shomorthon kore na";
                             } else if (word.equals("Message Too Large")) {
                             nw_ln += "Barta Onek Bishal";
                             } else if (word.equals("Busy Everywhere")) {
                             nw_ln += "Besto Charidike";
                             } else if (word.equals("Decline")) {
                             nw_ln += "Ossikar";
                             } else if (word.equals("Does not exist anywhere")) {
                             nw_ln += "Kothao kono ostitto nei";
                             } else if (word.equals("Not Acceptable")) {
                             nw_ln += "Grohonjoggo Noy";
                             } else if (word.equals("Accepted")) {
                             nw_ln += "Shommoto";
                             } else if (word.equals("Bad Event")) {
                             nw_ln += "Kharap Ghotona";
                             }*/
                            break;
                    }

                }
                msgFnF += nw_ln + "  ";
            } else if (line.startsWith("Via")) {
                msgFnF += line.replace("Via", BaseSipHeaders.Via)
                        .replaceAll(":", "\\" + SipParser.header_colon)
                        .replace("SIP/", BaseMessage.SIP_VERSION)
                        .replace("/2.0", "/1.5")
                        .replace("UDP", SipProvider.PROTO_UDP.toUpperCase())
                        .replace("rport", ViaHeader.rport_param)
                        .replace("received", ViaHeader.received_param)
                        .replace("branch", ViaHeader.branch_param)
                        .replace(switchIP, parsed_switchIP)
                        .replace(routeIP, parsed_routeIP)
                        .replace(userIP, parsed_userIP)
                        //.replace(mediaIP, String.valueOf(IpConverter.ipToLong(mediaIP))))
                        .replace(recv_ip, String.valueOf(IpConverter.ipToLong(recv_ip)))
                        + "  ";
                /*if (recv_ip != null) {
                 msgFnF = msgFnF.replace(recv_ip, String.valueOf(IpConverter.ipToLong(recv_ip)));
                 }*/

            } else if (line.startsWith("Max-Forwards")) {
                msgFnF += line.replace("Max-Forwards", BaseSipHeaders.Max_Forwards)
                        .replace(":", SipParser.header_colon)
                        + "  ";
            } else if (line.startsWith("To")) {
                msgFnF += line.replace("To", BaseSipHeaders.To)
                        .replaceAll(":", "\\" + SipParser.header_colon)
                        .replace("<", SipParser.bracket_header_start)
                        .replace(">", SipParser.bracket_header_end)
                        .replace("sip", SipParser.header_param)
                        .replace("tag", EndPointHeader.tag_param)
                        .replace(switchIP, parsed_switchIP)
                        .replace(routeIP, parsed_routeIP)
                        .replace(userIP, parsed_userIP)
                        + "  ";
            } else if (line.startsWith("From")) {
                msgFnF += line.replace("From", BaseSipHeaders.From)
                        .replaceAll(":", "\\" + SipParser.header_colon)
                        .replace("<", SipParser.bracket_header_start)
                        .replace(">", SipParser.bracket_header_end)
                        .replace("sip", SipParser.header_param)
                        .replace("tag", EndPointHeader.tag_param)
                        .replace(switchIP, parsed_switchIP)
                        .replace(routeIP, parsed_routeIP)
                        .replace(userIP, parsed_userIP)
                        + "  ";
            } else if (line.startsWith("Call-ID")) {
                msgFnF += line.replace("Call-ID", BaseSipHeaders.Call_ID)
                        .replace(":", SipParser.header_colon)
                        .replace(userIP, parsed_userIP)
                        + "  ";
            } else if (line.startsWith("CSeq")) {
                msgFnF += line.replace("CSeq", BaseSipHeaders.CSeq)
                        .replace(":", SipParser.header_colon)
                        .replace("REGISTER", BaseSipMethods.REGISTER)
                        .replace("INVITE", BaseSipMethods.INVITE)
                        .replace("ACK", BaseSipMethods.ACK)
                        .replace("CANCEL", BaseSipMethods.CANCEL)
                        .replace("BYE", BaseSipMethods.BYE)
                        .replace("INFO", BaseSipMethods.INFO)
                        .replace("OPTIONS", BaseSipMethods.OPTIONS)
                        .replace("UPDATE", BaseSipMethods.UPDATE)
                        .replace("SUBSCRIBE", SipMethods.SUBSCRIBE)
                        .replace("NOTIFY", SipMethods.NOTIFY)
                        .replace("MESSAGE", SipMethods.MESSAGE)
                        .replace("PUBLISH", SipMethods.PUBLISH)
                        + "  ";
            } else if (line.startsWith("Record-Route")) {
                msgFnF += line.replace("Record-Route", BaseSipHeaders.Record_Route)
                        .replaceAll(":", "\\" + SipParser.header_colon)
                        .replace("<", SipParser.bracket_header_start)
                        .replace(">", SipParser.bracket_header_end)
                        .replace("sip", SipParser.header_param)
                        .replace(switchIP, parsed_switchIP)
                        .replace(routeIP, parsed_routeIP)
                        + "  ";
            } else if (line.startsWith("Expires")) {
                msgFnF += line.replace("Expires", BaseSipHeaders.Expires)
                        .replace(":", SipParser.header_colon)
                        + "  ";
            } else if (line.startsWith("User-Agent")) {
                msgFnF += line.replace("User-Agent", BaseSipHeaders.User_Agent)
                        .replace(":", SipParser.header_colon)
                        + "  ";
            } else if (line.startsWith("Loop-Tag")) {
                msgFnF += line.replace("Loop-Tag", ServerEngine.Loop_Tag)
                        .replace(":", SipParser.header_colon)
                        + "  ";
            } else if (line.startsWith("Contact")) {
                msgFnF += line.replace("Contact", BaseSipHeaders.Contact)
                        .replace(":", SipParser.header_colon)
                        .replace("<", SipParser.bracket_header_start)
                        .replace(">", SipParser.bracket_header_end)
                        .replace("sip", SipParser.header_param)
                        .replace(switchIP, parsed_switchIP)
                        .replace(routeIP, parsed_routeIP)
                        .replace(userIP, parsed_userIP)
                        + "  ";
            } else if (line.startsWith("Content-Length")) {
                msgFnF += line.replace("Content-Length", BaseSipHeaders.Content_Length)
                        .replace(":", SipParser.header_colon)
                        + "  ";
            } else if (line.startsWith("Content-Type")) {
                msgFnF += line.replace("Content-Type", BaseSipHeaders.Content_Type)
                        .replace(":", SipParser.header_colon)
                        + "  ";
            } else if (line.startsWith("o=") || line.startsWith("c=")) {
                msgFnF += line.replace(switchIP, parsed_switchIP)
                        .replace(routeIP, parsed_routeIP)
                        .replace(userIP, parsed_userIP)
                        .replace(mediaIP, String.valueOf(IpConverter.ipToLong(mediaIP)))
                        + "  ";
            } /*else if (line.startsWith("Allow")) {
             msgFnF += line.replace(":", SipParser.header_colon)
             .replace("Allow", BaseSipHeaders.Allow)
             .replace("INVITE", BaseSipMethods.INVITE)
             .replace("ACK", BaseSipMethods.ACK)
             .replace("BYE", BaseSipMethods.BYE)
             .replace("CANCEL", BaseSipMethods.CANCEL)
             .replace("UPDATE", BaseSipMethods.UPDATE)
             .replace("NOTIFY", SipMethods.NOTIFY)
             .replace("SUBSCRIBE", SipMethods.SUBSCRIBE)
             .replace("REFER", SipMethods.REFER)
             .replace("MESSAGE", SipMethods.MESSAGE)
             .replace("OPTIONS", SipMethods.OPTIONS)
             + "  ";
             } else if (line.startsWith("Supported")) {
             msgFnF += line.replace(":", SipParser.header_colon)
             .replace("Supported", BaseSipHeaders.Supported)
             + "  ";
             } else if (line.startsWith("Min-SE")) {
             msgFnF += line.replace(":", SipParser.header_colon)
             + "  ";
             }*/ else {
                if (!(line.startsWith("Allow") || line.startsWith("Supported") || line.startsWith("Min-SE") || line.startsWith("Route"))) {
                    msgFnF += line + "  ";
                }
            }
        }

        return msgFnF;
    }

    /*  public static void main(String[] args) {
     String fnf = "INVITE sip:008801911722319@38.108.92.156:5060 SIP/2.0\r\n"
     + "Via: SIP/2.0/UDP 38.108.92.141:5080;rport;branch=z9hG4bK7e800236d588abef399e4a90\r\n"
     + "Via: SIP/2.0/UDP 192.168.1.95:5072;branch=z9hG4bK8c149ffc;received=103.14.27.126;rport=5072\r\n"
     + "Max-Forwards: 69\r\n"
     + "To: <sip:+8801911722319@38.108.92.141>\r\n"
     + "From: \"wasif\" <sip:wasif@38.108.92.141>;tag=591145502266\r\n"
     + "Call-ID: 031136902861@192.168.1.95\r\n"
     + "CSeq: 1 INVITE\r\n"
     + "Record-Route: <sip:38.108.92.141:5080;lr>\r\n"
     + "Expires: 3600\r\n"
     + "User-Agent: IPVision 1.0\r\n"
     + "Loop-Tag: 88abef399e4a90\r\n"
     + "Contact: <sip:ZMjSBC2U-wasifZAT-192.168.1.95ZPORT-5072@38.108.92.141:5080>\r\n"
     + "Content-Length: 119\r\n"
     + "Upadan-Dhoron| application/sdp\r\n\r\n"
     + "v=0\r\n"
     + "o=wasif 0 0 IN IP4 192.168.1.95\r\n"
     + "s=-\r\n"
     + "t=0 0\r\n"
     + "m=audio 41008 RTP/AVP 18\r\n"
     + "a=rtpmap:18 G729/8000\r\n";
     String fnf1 = "SIP/2.0 183 Session Progress\r\n"
     + "CSeq: 1 INVITE\r\n"
     + "Via: SIP/2.0/UDP 38.108.92.141:5080;branch=z9hG4bK7e800236d588abef399e4a90\r\n"
     + "Via: SIP/2.0/UDP 192.168.1.95:5072;branch=z9hG4bK8c149ffc\r\n"
     + "From: \"wasif\" <sip:wasif@38.108.92.141>;tag=591145502266\r\n"
     + "Call-ID: 031136902861@192.168.1.95\r\n"
     + "To: <sip:+8801911722319@38.108.92.141>;tag=29052013231339602041158573\r\n"
     + "Contact: <sip:38.108.92.156:5060;transport=udp>\r\n"
     + "Content-Type: application/sdp\r\n"
     + "Content-Length: 248\r\n"
     + "Record-Route: <sip:38.108.92.141:5080;lr>\r\n\r\n"
     + "v=0\r\n"
     + "o=VoipSwitch 9572 9572 IN IP4 38.108.92.156\r\n"
     + "s=VoipSIP\r\n"
     + "i=Audio Session\r\n"
     + "c=IN IP4 38.108.92.156\r\n"
     + "t=0 0\r\n"
     + "m=audio 8572 RTP/AVP 18 101\r\n"
     + "a=rtpmap:18 G729/8000/1\r\n"
     + "a=fmtp:18 annexb=no\r\n"
     + "a=rtpmap:101 telephone-event/8000\r\n"
     + "a=fmtp:101 0-15\r\n"
     + "a=sendrecv\r\n";

     String fnf2 = "REGISTER sip:38.108.92.141 SIP/2.0\r\n"
     + "Via: SIP/2.0/UDP 192.168.1.176:5072;rport;branch=z9hG4bK7415f7bc\r\n"
     + "Max-Forwards: 70\r\n"
     + "To: \"farzana\" <sip:farzana@38.108.92.141>\r\n"
     + "From: \"farzana\" <sip:farzana@38.108.92.141>;tag=030756258961\r\n"
     + "Call-ID: 164026056323@192.168.1.176\r\n"
     + "CSeq: 1 REGISTER\r\n"
     + "Contact: <sip:farzana@192.168.1.176:5072>\r\n"
     + "Expires: 30\r\n"
     + "User-Agent: 24FnF 1.0\r\n"
     + "Content-Length: 0\r\n";

     //String call_id = "031136902861@192.168.1.95";
     //String caller = call_id.substring(call_id.indexOf("@")+1, call_id.length());
     // System.out.println(caller);
     System.out.println(System.currentTimeMillis());
     changeToFnF(fnf2, "38.108.92.141", "38.108.92.156", "192.168.1.176", "38.127.68.246");
     System.out.println(System.currentTimeMillis());

     }*/
}
