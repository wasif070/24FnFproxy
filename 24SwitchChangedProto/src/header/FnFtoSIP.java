/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package header;

import java.util.StringTokenizer;
import local.server.ServerEngine;
import org.zoolu.sip.header.BaseSipHeaders;
import org.zoolu.sip.header.EndPointHeader;
import org.zoolu.sip.header.ViaHeader;
import org.zoolu.sip.message.BaseMessage;
import org.zoolu.sip.message.BaseSipMethods;
import org.zoolu.sip.message.SipMethods;
import org.zoolu.sip.provider.SipParser;
import org.zoolu.sip.provider.SipProvider;
import org.zoolu.sip.utils.IpConverter;

/**
 *
 * @author Wasif Islam
 */
public class FnFtoSIP {

    // public static String recv_long_ip = "-418505857";
    public static String recv_long_ip;
    static String parsed_switchLongIP;
    static String parsed_routeLongIP;
    static String parsed_userLongIP;

    public static String changeToSIP(String msg, String switchLongIP, String routeLongIP, String userLongIP, String mediaLongIP) {
        System.out.println("------------------------------- Inside changeToSIP() --------------------- ");
        String msgSIP = "";
        while (msg.length() > 0) {
            int space_index = msg.indexOf("  ");
            if (space_index == -1) {
                break;
            }
            String line = msg.substring(0, space_index);

            msg = msg.substring(space_index + 2, msg.length());

            try {
                parsed_switchLongIP = IpConverter.longToIp(Long.parseLong(switchLongIP));
            } catch (Exception e) {
                parsed_switchLongIP = switchLongIP;
            }
            try {
                parsed_routeLongIP = IpConverter.longToIp(Long.parseLong(routeLongIP));
            } catch (Exception e) {
                parsed_routeLongIP = routeLongIP;
            }
            try {
                parsed_userLongIP = IpConverter.longToIp(Long.parseLong(userLongIP));
            } catch (Exception e) {
                parsed_userLongIP = userLongIP;
            }

            if (line.startsWith(BaseSipMethods.REGISTER) || line.startsWith(BaseSipMethods.INVITE) || line.startsWith(BaseSipMethods.ACK) || line.startsWith(BaseSipMethods.CANCEL) || line.startsWith(BaseSipMethods.BYE) || line.startsWith(BaseSipMethods.INFO) || line.startsWith(BaseSipMethods.OPTIONS) || line.startsWith(BaseSipMethods.UPDATE)) {
                msgSIP += line.replace(BaseSipMethods.REGISTER, "REGISTER")
                        .replace(BaseSipMethods.INVITE, "INVITE")
                        .replace(BaseSipMethods.ACK, "ACK")
                        .replace(BaseSipMethods.CANCEL, "CANCEL")
                        .replace(BaseSipMethods.BYE, "BYE")
                        .replace(BaseSipMethods.INFO, "INFO")
                        .replace(BaseSipMethods.OPTIONS, "OPTIONS")
                        .replace(BaseSipMethods.UPDATE, "UPDATE")
                        .replace(SipParser.header_param, "sip")
                        .replace(SipParser.header_colon, ":")
                        .replace(BaseMessage.SIP_VERSION, "SIP/")
                        .replace("/1.5", "/2.0")
                        .replace(switchLongIP, parsed_switchLongIP)
                        .replace(routeLongIP, parsed_routeLongIP)
                        .replace(userLongIP, parsed_userLongIP)
                        + "\r\n";
            } else if (line.startsWith(BaseMessage.SIP_VERSION)) {


                int i = 0;
                String nw_ln = "";
                while (line.length() > 0) {
                    i++;
                    String word;
                    int index = line.indexOf(" ");
                    if (i < 3) {
                        word = line.substring(0, index);
                        line = line.substring(index + 1, line.length());
                    } else {
                        word = line.substring(0, line.length());
                        line = "";
                    }
                    switch (i) {
                        case 1:
                            nw_ln = "SIP/2.0 ";
                            break;
                        case 2:
                            try {
                                int err_code = Integer.parseInt(word);
                                nw_ln += String.valueOf(err_code - 1000) + " ";
                                // is an integer!
                            } catch (NumberFormatException e) {
                                // not an integer!
                            }
                            break;
                        case 3:
                            nw_ln += word;
                            /*if (word.equals("Ovvontorin somossa")) {
                             nw_ln += "Internal error";
                             } else if (word.equals("Chesta")) {
                             nw_ln += "Trying";
                             } else if (word.equals("Dhonito")) {
                             nw_ln += "Ringing";
                             } else if (word.equals("Barta Ogroshor kora hoyese")) {
                             nw_ln += "Call Is Being Forwarded";
                             } else if (word.equals("Shariboddho")) {
                             nw_ln += "Queued";
                             } else if (word.equals("Shomoy Ogrogoti")) {
                             nw_ln += "Session Progress";
                             } else if (word.equals("Assa")) {
                             nw_ln += "OK";
                             } else if (word.equals("Ekadhik Posondo")) {
                             nw_ln += "Multiple Choices";
                             } else if (word.equals("Shorano Sthayivabe")) {
                             nw_ln += "Moved Permanently";
                             } else if (word.equals("Shorano Shamoikvabe")) {
                             nw_ln += "Moved Temporarily";
                             } else if (word.equals("Bebohar Protinidhi")) {
                             nw_ln += "Use Proxy";
                             } else if (word.equals("Bikolpo Sheba")) {
                             nw_ln += "Alternative Service";
                             } else if (word.equals("Ku Prostab")) {
                             nw_ln += "Bad Request";
                             } else if (word.equals("Oboidho")) {
                             nw_ln += "Unauthorized";
                             } else if (word.equals("Porisodh Korun")) {
                             nw_ln += "Payment Required";
                             } else if (word.equals("Nisiddho")) {
                             nw_ln += "Forbidden";
                             } else if (word.equals("Pai Nai")) {
                             nw_ln += "Not Found";
                             } else if (word.equals("Poddhoti Boidho Noy")) {
                             nw_ln += "Method Not Allowed";
                             } else if (word.equals("Grohonjoggo Noy")) {
                             nw_ln += "Not Acceptable";
                             } else if (word.equals("Protinidhi Proman Abosshok")) {
                             nw_ln += "Proxy Authentication Required";
                             } else if (word.equals("Onurodh Somoysesh")) {
                             nw_ln += "Request Timeout";
                             } else if (word.equals("Biday")) {
                             nw_ln += "Proxy Authentication Required";
                             } else if (word.equals("Onurodh Ostitto Onek Boro")) {
                             nw_ln += "Proxy Authentication Required";
                             } else if (word.equals("Onurodh-URI Onek Boro")) {
                             nw_ln += "Request-URI Too Large";
                             } else if (word.equals("Oshomorthito Media Nomuna")) {
                             nw_ln += "Unsupported Media Type";
                             } else if (word.equals("Oshomorthito URI Prokolpo")) {
                             nw_ln += "Proxy Authentication Required";
                             } else if (word.equals("Protinidhi Proman Abosshok")) {
                             nw_ln += "Unsupported URI Scheme";
                             } else if (word.equals("Kharap Proshar")) {
                             nw_ln += "Bad Extension";
                             } else if (word.equals("Proshar Dorkar")) {
                             nw_ln += "Extension Required";
                             } else if (word.equals("Biroti Khub Kom")) {
                             nw_ln += "Interval Too Brief";
                             } else if (word.equals("Shamoikvabe prappo noy")) {
                             nw_ln += "Temporarily not available";
                             } else if (word.equals("Ahoban Pa/Lenden Somvob Noy")) {
                             nw_ln += "Call Leg/Transaction Does Not Exist";
                             } else if (word.equals("Ghuron Shonakto")) {
                             nw_ln += "Loop Detected";
                             } else if (word.equals("Onek Besi Hops")) {
                             nw_ln += "Too Many Hops";
                             } else if (word.equals("Thikana Oshompurno")) {
                             nw_ln += "Address Incomplete";
                             } else if (word.equals("Osposhto")) {
                             nw_ln += "Ambiguous";
                             } else if (word.equals("Besto Ekhane")) {
                             nw_ln += "Busy Here";
                             } else if (word.equals("Onurodh Batil")) {
                             nw_ln += "Request Terminated";
                             } else if (word.equals("Grohonjoggo Noy")) {
                             nw_ln += "Not Acceptable Here";
                             } else if (word.equals("Onurodh Multobi")) {
                             nw_ln += "Request Pending";
                             } else if (word.equals("Durboddho")) {
                             nw_ln += "Undecipherable";
                             } else if (word.equals("Onurodh Multobi")) {
                             nw_ln += "Request Pending";
                             } else if (word.equals("Ovvontorin Server Truti")) {
                             nw_ln += "Internal Server Error";
                             } else if (word.equals("Proyog Hoyni")) {
                             nw_ln += "Not Implemented";
                             } else if (word.equals("Kharap Dorja")) {
                             nw_ln += "Bad Gateway";
                             } else if (word.equals("Sheba Nei")) {
                             nw_ln += "Service Unavailable";
                             } else if (word.equals("Server Somoy-sesh")) {
                             nw_ln += "Server Time-out";
                             } else if (word.equals("24FNF Version Shomorthon kore na")) {
                             nw_ln += "SIP Version not supported";
                             } else if (word.equals("Barta Onek Bishal")) {
                             nw_ln += "Message Too Large";
                             } else if (word.equals("Besto Charidike")) {
                             nw_ln += "Busy Everywhere";
                             } else if (word.equals("Ossikar")) {
                             nw_ln += "Decline";
                             } else if (word.equals("Kothao kono ostitto nei")) {
                             nw_ln += "Does not exist anywhere";
                             } else if (word.equals("Grohonjoggo Noy")) {
                             nw_ln += "Not Acceptable";
                             } else if (word.equals("Shommoto")) {
                             nw_ln += "Accepted";
                             } else if (word.equals("Kharap Ghotona")) {
                             nw_ln += "Bad Event";
                             }*/
                            break;
                    }

                }
                msgSIP += nw_ln + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.Via)) {
                msgSIP += line.replace(BaseSipHeaders.Via, "Via")
                        .replaceAll("\\" + SipParser.header_colon, ":")
                        .replace(BaseMessage.SIP_VERSION, "SIP/")
                        .replace("/1.5", "/2.0")
                        .replace(SipProvider.PROTO_UDP.toUpperCase(), "UDP")
                        .replace(ViaHeader.rport_param, "rport")
                        .replace(ViaHeader.received_param, "received")
                        .replace(ViaHeader.branch_param, "branch")
                        .replace(switchLongIP, parsed_switchLongIP)
                        .replace(routeLongIP, parsed_routeLongIP)
                        .replace(userLongIP, parsed_userLongIP)
                        //.replace(mediaLongIP, IpConverter.longToIp(Long.parseLong(mediaLongIP)))
                        .replace(recv_long_ip, IpConverter.longToIp(Long.parseLong(recv_long_ip)))
                        + "\r\n";
                /* if (recv_long_ip != null) {
                 msgSIP = msgSIP.replace(recv_long_ip, IpConverter.longToIp(Long.parseLong(recv_long_ip)));
                 }*/
            } else if (line.startsWith(BaseSipHeaders.Max_Forwards)) {
                msgSIP += line.replace(BaseSipHeaders.Max_Forwards, "Max-Forwards")
                        .replace(SipParser.header_colon, ":")
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.To)) {
                msgSIP += line.replace(BaseSipHeaders.To, "To")
                        .replaceAll("\\" + SipParser.header_colon, ":")
                        .replace(SipParser.bracket_header_start, "<")
                        .replace(SipParser.bracket_header_end, ">")
                        .replace(SipParser.header_param, "sip")
                        .replace(EndPointHeader.tag_param, "tag")
                        .replace(switchLongIP, parsed_switchLongIP)
                        .replace(routeLongIP, parsed_routeLongIP)
                        .replace(userLongIP, parsed_userLongIP)
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.From)) {
                msgSIP += line.replace(BaseSipHeaders.From, "From")
                        .replaceAll("\\" + SipParser.header_colon, ":")
                        .replace(SipParser.bracket_header_start, "<")
                        .replace(SipParser.bracket_header_end, ">")
                        .replace(SipParser.header_param, "sip")
                        .replace(EndPointHeader.tag_param, "tag")
                        .replace(switchLongIP, parsed_switchLongIP)
                        .replace(routeLongIP, parsed_routeLongIP)
                        .replace(userLongIP, parsed_userLongIP)
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.Call_ID)) {
                msgSIP += line.replace(BaseSipHeaders.Call_ID, "Call-ID")
                        .replace(SipParser.header_colon, ":")
                        .replace(userLongIP, parsed_userLongIP)
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.CSeq)) {
                msgSIP += line.replace(BaseSipHeaders.CSeq, "CSeq")
                        .replace(SipParser.header_colon, ":")
                        .replace(BaseSipMethods.REGISTER, "REGISTER")
                        .replace(BaseSipMethods.INVITE, "INVITE")
                        .replace(BaseSipMethods.ACK, "ACK")
                        .replace(BaseSipMethods.CANCEL, "CANCEL")
                        .replace(BaseSipMethods.BYE, "BYE")
                        .replace(BaseSipMethods.INFO, "INFO")
                        .replace(BaseSipMethods.OPTIONS, "OPTIONS")
                        .replace(BaseSipMethods.UPDATE, "UPDATE")
                        .replace(SipMethods.SUBSCRIBE, "SUBSCRIBE")
                        .replace(SipMethods.NOTIFY, "NOTIFY")
                        .replace(SipMethods.MESSAGE, "MESSAGE")
                        .replace(SipMethods.PUBLISH, "PUBLISH")
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.Record_Route)) {
                msgSIP += line.replace(BaseSipHeaders.Record_Route, "Record-Route")
                        .replaceAll("\\" + SipParser.header_colon, ":")
                        .replace(SipParser.bracket_header_start, "<")
                        .replace(SipParser.bracket_header_end, ">")
                        .replace(SipParser.header_param, "sip")
                        .replace(switchLongIP, parsed_switchLongIP)
                        .replace(routeLongIP, parsed_routeLongIP)
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.Expires)) {
                msgSIP += line.replace(BaseSipHeaders.Expires, "Expires")
                        .replace(SipParser.header_colon, ":")
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.User_Agent)) {
                msgSIP += line.replace(BaseSipHeaders.User_Agent, "User-Agent")
                        .replace(SipParser.header_colon, ":")
                        + "\r\n";
            } else if (line.startsWith(ServerEngine.Loop_Tag)) {
                msgSIP += line.replace(ServerEngine.Loop_Tag, "Loop-Tag")
                        .replace(SipParser.header_colon, ":")
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.Contact)) {
                msgSIP += line.replace(BaseSipHeaders.Contact, "Contact")
                        .replace(SipParser.header_colon, ":")
                        .replace(SipParser.bracket_header_start, "<")
                        .replace(SipParser.bracket_header_end, ">")
                        .replace(SipParser.header_param, "sip")
                        .replace(switchLongIP, parsed_switchLongIP)
                        .replace(routeLongIP, parsed_routeLongIP)
                        .replace(userLongIP, parsed_userLongIP)
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.Content_Length)) {
                msgSIP += line.replace(BaseSipHeaders.Content_Length, "Content-Length")
                        .replace(SipParser.header_colon, ":")
                        + "\r\n";
            } else if (line.startsWith(BaseSipHeaders.Content_Type)) {
                msgSIP += line.replace(BaseSipHeaders.Content_Type, "Content-Type")
                        .replace(SipParser.header_colon, ":")
                        + "\r\n";
            } else if (line.startsWith("o=") || line.startsWith("c=")) {
                msgSIP += line.replace(switchLongIP, IpConverter.longToIp(Long.parseLong(switchLongIP)))
                        .replace(routeLongIP, parsed_routeLongIP)
                        .replace(userLongIP, parsed_userLongIP)
                        .replace(mediaLongIP, IpConverter.longToIp(Long.parseLong(mediaLongIP)))
                        + "\r\n";
            } /*else if (line.startsWith(BaseSipHeaders.Allow)) {
             msgSIP += line.replace(SipParser.header_colon, ":")
             .replace(BaseSipHeaders.Allow, "Allow")
             .replace(BaseSipMethods.INVITE, "INVITE")
             .replace(BaseSipMethods.ACK, "ACK")
             .replace(BaseSipMethods.BYE, "BYE")
             .replace(BaseSipMethods.CANCEL, "CANCEL")
             .replace(BaseSipMethods.UPDATE, "UPDATE")
             .replace(SipMethods.NOTIFY, "NOTIFY")
             .replace(SipMethods.SUBSCRIBE, "SUBSCRIBE")
             .replace(SipMethods.REFER, "REFER")
             .replace(SipMethods.MESSAGE, "MESSAGE")
             .replace(SipMethods.OPTIONS, "OPTIONS")
             + "\r\n";
             } else if (line.startsWith(BaseSipHeaders.Supported)) {
             msgSIP += line.replace(SipParser.header_colon, ":")
             .replace(BaseSipHeaders.Supported, "Supported")
             + "\r\n";
             } else if (line.startsWith("Min-SE")) {
             msgSIP += line.replace(SipParser.header_colon, ":")
             + "\r\n";
             } */ else {
                if (!(line.startsWith(BaseSipHeaders.Allow) || line.startsWith(BaseSipHeaders.Supported) || line.startsWith("Min-SE"))) {
                    msgSIP += line + "\r\n";
                }
            }
        }
        return msgSIP;

    }

    /* public static void main(String[] args) {
     String fnf = "AMONTRON 24fnf|008801911722319@-1502847843|5060 24FNF/1.5  Maddhom| 24FNF/1.5/BTN -1502847858|5080;dport;shakha=z9hG4bK7e800236d588abef399e4a90  Maddhom| 24FNF/1.5/BTN 1084752224|5072;shakha=z9hG4bK8c149ffc;grihito=-418505857;dport=5072  Sorboccho-Ogrogoti| 69  Prapok| (24fnf|+8801911722319@-1502847858)  Prerok| wasif (24fnf|wasif@-1502847858);nirdeshok=591145502266  Porichiti| 031136902861@1084752224  Krom| 1 AMONTRON  Nothi-Rasta| (24fnf|-1502847858|5080;lr)  Meyad| 3600  Byboharkari| IPVision 1.0  Britto-Nirdeshok| 88abef399e4a90  Thikana| (24fnf|ZMjSBC2U-wasifZAT-1084752224ZPORT-5072@-1502847858|5080)  Upadan-Doirgho| 119  Upadan-Dhoron| application/sdp    v=0  o=wasif 0 0 IN IP4 1084752224  s=-  t=0 0  m=audio 41008 RTP/AVP 18  a=rtpmap:18 G729/8000  ";
     String fnf1 = "24FNF/1.5 1183 Shomoy Ogrogoti  Krom| 1 AMONTRON  Maddhom| 24FNF/1.5/BTN -1502847858|5080;shakha=z9hG4bK7e800236d588abef399e4a90  Maddhom| 24FNF/1.5/BTN 1084752224|5072;shakha=z9hG4bK8c149ffc  Prerok| wasif (24fnf|wasif@-1502847858);nirdeshok=591145502266  Porichiti| 031136902861@1084752224  Prapok| (24fnf|+8801911722319@-1502847858);nirdeshok=29052013231339602041158573  Thikana| (24fnf|-1502847843|5060;transport=udp)  Upadan-Dhoron| application/sdp  Upadan-Doirgho| 248  Nothi-Rasta| (24fnf|-1502847858|5080;lr)    v=0  o=VoipSwitch 9572 9572 IN IP4 -1502847843  s=VoipSIP  i=Audio Session  c=IN IP4 -1502847843  t=0 0  m=audio 8572 RTP/AVP 18 101  a=rtpmap:18 G729/8000/1  a=fmtp:18 annexb=no  a=rtpmap:101 telephone-event/8000  a=fmtp:101 0-15  a=sendrecv  ";
     String fnf2 = "AMONTRON 24fnf|008801911722319@38.108.92.156:5060 24FNF/1.5  "
     + "Maddhom| 24FNF/1.5/BTN -1502847858|5080;dport;shakha=z9hG4bK9f7e03d1f9b7d5a4ed9c188b  "
     + "Maddhom| 24FNF/1.5/BTN 1084752242|5072;shakha=z9hG4bKb3c685d6;grihito=-418505857;dport=5072  "
     + "Sorboccho-Ogrogoti| 69  "
     + "Prapok| (24fnf|+8801911722319@-1502847858)  "
     + "Prerok| \"zzzz\" (24fnf|zzzz@-1502847858);nirdeshok=395826321456  "
     + "Porichiti| 142690410560@1084752242  "
     + "Krom| 1 AMONTRON  "
     + "Nothi-Rasta| (24fnf|-1502847858|5080;lr)  "
     + "Meyad| 3600  "
     + "Byboharkari| IPVision 1.0  "
     + "Britto-Nirdeshok| b7d5a4ed9c188b  "
     + "Thikana| (24fnf|ZMjSBC2U-zzzzZAT-1084752242ZPORT-5072@-1502847858|5080)  "
     + "Upadan-Doirgho| 118  "
     + "Upadan-Dhoron| application/sdp    "
     + "v=0  "
     + "o=zzzz 0 0 IN IP4 1084752242  "
     + "s=-  "
     + "c=IN IP4 -1501608713  "
     + "t=0 0  "
     + "m=audio 42000 RTP/AVP 18  "
     + "a=rtpmap:18 G729/8000  ";

     changeToSIP(fnf2, "-1502847858", "-1502847843", "1084752242", "-1501608713");

     int space_index = "asadas".indexOf("  ");

     }*/
}
